import React from 'react';
import {Alert, StyleSheet, Image, View, Text, TextInput, ActivityIndicator, TouchableOpacity, KeyboardAvoidingView } from "react-native";
import Network from "./services/NetworkRequest";
import Constant from "./services/Constants";
 
export default class ForgetPasswordScreen extends React.Component {
    state = {
        email: '',
        loading: false
    };
    handlePasswordReset = () => {
      let {email} = this.state;
      let {navigation} = this.props;
      if(email == '') {
        return Alert.alert('Error', 'Enter Email-ID');
      }
      this.setState({
        loading: true
      });
      Network.forgetPassword(email).then(resp => {
        console.log('resp:------>', resp);
        this.setState({
          loading: false
        });
        if(resp.success) {
          Alert.alert('Success', "Email verification link has been sent to your registered email id");
          navigation.navigate('Login');
        } else {
          Alert.alert('Error', resp.errors.error);
        }
      }, err => {
        this.setState({
          loading: false
        });
        console.log(err);
      }).catch(error => {
        this.setState({
          loading: false
        });
        console.log(error);
      });
    }

    render() {
      let {loading} = this.state;
        return(
            <View style={Styles.container}>
              <KeyboardAvoidingView behavior="height" enabled>
                <View style={Styles.container}>
                  <View style={Styles.inputHolder}>
                    <View style={{alignItems: 'center', justifyContent: 'center', alignSelf: 'center', padding: 30}}>
                      <Image style={{width: 260, height: 60}} source={require('./assets/logo.png')}/>
                    </View>
                    <TextInput style={Styles.textInputStyle} placeholder="Enter Registered Email-ID" keyboardType="email-address" onChangeText={(email) => this.setState({email})}/>
                    <TouchableOpacity style={Styles.SubmitButtonStyle} activeOpacity={0.5} onPress={this.handlePasswordReset}>
                      <Text style={Styles.buttonTextStyle}> RESET PASSWORD </Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity style={Styles.CancelStyleButton} activeOpacity={0.5} onPress={() => this.setState({view: 'mobile'})}>
                      <Text style={Styles.cancelbuttonTextStyle}>SIGN UP With MOBILE NUMBER</Text>
                    </TouchableOpacity> */}
                  </View>
                </View>
                {(loading) ? 
                <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignContent: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0)'}}>
                <ActivityIndicator size="large" color="red"/>
              </View> : null}
              </KeyboardAvoidingView>
            </View>
        );
    }
}

const Styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "white",
      alignItems: "center",
      justifyContent: "center"
    },
    textInputStyle: {
      width: '100%',
      marginTop: 10,
      padding: 5,
      paddingLeft: 15,
      borderWidth: 1,
      borderRadius: 5,
      borderColor: Constant.borderGrey,
    },
    inputHolder: {
      flex: 1,
      width: '100%',
      alignItems: "center",
      justifyContent: "center"
    },
    buttonTextStyle: {
      color: "#FFFFFF",
      textAlign: "center",
      fontWeight: "800"
    },
    SubmitButtonStyle: {
      width: '80%',
      marginTop: 10,
      paddingTop: 10,
      paddingLeft: 20,
      paddingRight: 20,
      paddingBottom: 15,
      backgroundColor: Constant.themeColor,
      marginTop: 40,
      borderRadius: 20,
      borderWidth: 1,
      borderColor: "#fff"
    },
    textButtonStyle: {
      width: 1000,
      height: 40,
      marginTop: 10,
      paddingTop: 10,
      paddingBottom: 10,
      backgroundColor: "#75d472",
      borderRadius: 20,
      borderWidth: 1,
      borderColor: "#fff"
    },
   
  });