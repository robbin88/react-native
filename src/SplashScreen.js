import React from 'react';
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  View,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class SplashScreen extends React.Component {
    componentDidMount() {
        this._bootstrapAsync();
    }
    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('userData');
        this.props.navigation.navigate(userToken ? 'App' : 'Auth');
    };
    render() {
        return(
            <View style={Styles.container}>
                <View style={{alignContent: 'center', justifyContent: 'center', flex: 1}}>
                    <Image style={{width: 280, height: 60, alignSelf: 'center'}} source={require('./assets/logo.png')}/>
                    <ActivityIndicator/>
                </View>
            </View>
        );
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        width: '100%',
    }
});