import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import SplashScreen from "./SplashScreen";
import LoginRouter from './LoginRouter';
import Routes from "./Routes";

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: SplashScreen,
      App: Routes,
      Auth: LoginRouter,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )
);