import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import BeneficiaryList from "./Beneficiary/ListAllBeneficiary";
import AddNewBeneficiary from "./Beneficiary/AddNewBeneficiary";

const BeneficiaryStack = createStackNavigator({
    List: {screen: BeneficiaryList},
    Add: {screen: AddNewBeneficiary},
}, {
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
});

export default createAppContainer(BeneficiaryStack);
