import React from 'react';
import Network from "../../services/NetworkRequest";
import {ActivityIndicator, View, Alert, Image, Text, TextInput, StyleSheet, SafeAreaView, ScrollView, Switch, KeyboardAvoidingView, TouchableOpacity, Picker} from 'react-native';
import Constant from '../../services/Constants';
import CurrencyPicker from "../../components/CurrencyPicker";
import ProviderPicker from "../../components/ProviderPicker";
import BankPicker from "../../components/BankPicker";


const initialState = {
	formType: false,
	allCurrencies: null,
	allBanks: null,
	otherBanks: '',
	allProviders: null,
	selectedBank: '',
	selectedCurrency: '',
	selectedProvider: '',
	first_name: "", 
	relation: "",
	last_name: "",
	currency: "",
	city: "",
	email: "",
	user_id: "",
	mobile_number: "",
	bank_id: "",
	bank_name: "",
	transaction_type: "",
	bank_account_type: "",
	bank_account_number: "",
	c_bank_account_number: "",
	loading: false,
};

let filledInitialState = initialState;
export default class AddNewBeneficiary extends React.Component {
	state = initialState;

	componentDidMount() {
		this.getAllCurrencies();
	}

	getAllCurrencies = () => {
		Network.getAllCurrencies().then(resp => {
			console.log('getAllCurrencies:------>', resp);
			// if(resp.success) {
			this.setState({
				allCurrencies: resp.data.payout_currencies
			});
			// } else {
				// Alert.alert('Error');
			// }
		}, err => {
			console.log(err);
		});
	}

	handleCurrencySelection = (item, index) => {
		this.setState({
			selectedCurrency: item,
			currency: item.destination_currency,
		});
		Network.getCurrencyId(item.destination_currency).then(resp => {
			let currencyId = resp.data.currency.id;
			if(currencyId){
				if(this.state.formType) {
					//bank case
					Network.getBankForSelectedCurrency(currencyId).then(resp => {
						console.log('get getBankForSelectedCurrency resp----->:', resp);
						this.setState({
							allBanks: resp.data.banks
						})
					}, err => {
						console.log(err);
					});
				} else {
					//Mobile
					Network.getAllProviders(currencyId).then(resp => {
						console.log('get getAllProviders resp:----->', resp);
						if (resp.data.providers){
							this.setState({ allProviders: resp.data.providers });
					    }
				}, err => {
						console.log(err);
					});
				}
			} else {
				Alert.alert('Error', 'Unable to get selected currency');
			}
		}, err => {
			console.log(err);
		});		
	}

	handleAddBeneficiary = () => {
		let {
			selectedProvider, 
			selectedCurrency, 
			formType, 
			c_bank_account_number, 
			bank_account_number,
			first_name,
			last_name,
			city
	 	} = this.state;
		if(first_name == '') {
			Alert.alert('Error', 'Please enter first name');
			return;
		}
		if(last_name == '') {
			Alert.alert('Error', 'Please enter last name');
			return;
		}
		if(city == '') {
			Alert.alert('Error', 'Please enter city name');
			return;
		}
		if(selectedCurrency == '' || selectedCurrency == null) {
			return Alert.alert('Error', 'Please Select Currency');
		}
		if((selectedProvider == null ||  selectedProvider == '') && !formType) {
			return Alert.alert('Error', 'Please Select Provider');
		}
		if((c_bank_account_number == '' || c_bank_account_number == null || bank_account_number == '' || bank_account_number == null) && formType) {
			Alert.alert('Error', 'Please fill account number');
			return;
		}
		if((c_bank_account_number !== bank_account_number) && formType) {
			Alert.alert('Error', 'Please check account number');
			return;
		}
		this.setState({loading: true});
		Network.addBenificiary(this.state).then(resp => {
			console.log(resp);
			if(resp.success) {
				this.setState({loading: false});
				Alert.alert("Success",'Beneficiary Added Successfully');
				this.props.navigation.navigate('List', {reload: true});
				// that.setState(initialState);
			} else {
				this.setState({loading: false});
				Alert.alert('error', resp.errors.error);
			}
		}, err => {
			this.setState({loading: false});
			Alert.alert('Something went wrong please try again');
			console.log('handleAddBeneficiary--->', err);
		});
	}
	handleSelectedCurrency = (value, status) => {
		if(!status && value != null) {
			this.setState({
				selectedCurrency: value
			}, () => {
				this.handleCurrencySelection(value);
			});
		} else {
			this.setState({
				selectedCurrency: null
			});
		}
	}
	handleSelectedProvider = (value, status) => {
		if(!status && value != null) {
			this.setState({
				selectedProvider: value
			}, () => {
				this.handleCurrencySelection(value);
			});
		} else {
			this.setState({
				selectedProvider: null
			});
		}
	}
	handleSelectedBank = (value, status) => {
		if(!status && value != null) {
			this.setState({
				selectedBank: value
			});
		} else {
			this.setState({
				selectedBank: null
			});
		}
	}
	render() {
		let { loading } = this.state;
		const { allCurrencies, allBanks, formType, allProviders } = this.state;
		return(
			<SafeAreaView style={{flex: 1}}>
				<View style={{padding: 10, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#888', elevation: 1}}>
					<TouchableOpacity onPress={() => this.props.navigation.navigate('List')}>
						<Image style={Styles.iconStyle} source={require('../../assets/back-icon.png')}/>
					</TouchableOpacity>
					<Text style={Styles.padding5}>Add New Beneficiary</Text>
					{/* <Image style={{ height: 30, width: 30,alignSelf:"flex-end" }} source={require('../../assets/beneficiary.png')}/> */}
				</View> 
				<View style={[Styles.topImage,{borderBottomWidth: 1,borderBottomColor: '#888881'}]}>
					<Image style={{ height: 100, width: 100, alignSelf: 'center', marginTop: 10 }} source={require('../../assets/beneficiary.png')}/>
					<View style={Styles.addViewStyle}>
						<Text style={Styles.addTextStyle} >Add Beneficiary</Text>
						<View style={{flexDirection: 'row'}}> 
							<Text style={{color: 'red'}}>* Mandatory Fields</Text>
						</View>
					</View>
				</View>
				<View style={{flex: 1}}>
				<KeyboardAvoidingView enabled keyboardVerticalOffset={-500} behavior="padding">
					<ScrollView>
						<View style={Styles.form}>
							<View style={Styles.formField}>
								<Text style={Styles.inputLabel}>Select Beneficiary Type</Text>
								<View style={{flexDirection: 'row', padding: 5}}>
									<Switch 
										value={formType} 
										onChange={(formType) => {
											this.setState({
												formType: !this.state.formType,
												selectedCurrency: null,
												selectedBank: null,
												selectedProvider: null
											});
										}}>
									</Switch>
									{(formType) 
									?<Text style={Styles.padding5}>Bank</Text> 
									:<Text style={Styles.padding5}>Cash</Text>}
								</View>
							</View>
							<View style={Styles.formField}>
								<View style={{flexDirection: 'row'}}><Text style={Styles.inputLabel}>First Name</Text><Text style={{color: 'red', marginLeft: 5}}>*</Text></View>
								<TextInput onChangeText={(first_name) => this.setState({first_name: first_name})} style={Styles.formInput}/>
							</View>
							<View style={Styles.formField}>
								<View style={{flexDirection: 'row'}}><Text style={Styles.inputLabel}>Last Name</Text><Text style={{color: 'red', marginLeft: 5}}>*</Text></View>
								<TextInput onChangeText={(last_name) => this.setState({last_name: last_name})} style={Styles.formInput}/>
							</View>
							<View style={Styles.formField}>
								<View style={{flexDirection: 'row'}}><Text style={Styles.inputLabel}>Email Address</Text></View>
								<TextInput keyboardType="email-address" autoCapitalize="none" onChangeText={(email) => this.setState({email: email})} style={Styles.formInput}/>
							</View>
							<View style={Styles.formField}>
								<View style={{flexDirection: 'row'}}><Text style={Styles.inputLabel}>Phone Number</Text></View>
								<TextInput keyboardType="numeric" onChangeText={(mobile_number) => this.setState({mobile_number: mobile_number})} style={Styles.formInput}/>
							</View>
							<View style={Styles.formField}>
								<View style={{flexDirection: 'row'}}><Text style={Styles.inputLabel}>Currency</Text><Text style={{color: 'red', marginLeft: 5}}>*</Text></View>
								{/* IOs Picker */}
								<CurrencyPicker selectedValue={this.handleSelectedCurrency} onChange={this.handleCurrencySelection} data={allCurrencies}/>
								{/* Android Picker */}
								{/* <Picker selectedValue={this.state.selectedCurrency} style={{height: 50, width: '100%'}} onValueChange={this.handleCurrencySelection}>
									<Picker.Item disabled={true} label="Select Currency" value="null"/>
									{(allCurrencies) ? allCurrencies.map((item, index) => (
										<Picker.Item key={index} label={item.destination_currency} value={item}/>
									)) : null}
								</Picker> */}
							</View>
							<View style={Styles.formField}>		
								<View style={{flexDirection: 'row'}}><Text style={Styles.inputLabel}>City</Text><Text style={{color: 'red', marginLeft: 5}}>*</Text></View>
								<TextInput onChangeText={(city) => this.setState({city: city})} style={Styles.formInput}/>
							</View>
							{(this.state.formType) ? 
								<View>
									<View style={Styles.formField}>
										<View style={{flexDirection: 'row'}}><Text style={Styles.inputLabel}>Bank</Text><Text style={{color: 'red', marginLeft: 5}}>*</Text></View>
										{/* IOs Picker */}
										<BankPicker selectedValue={this.handleSelectedBank} onChange={this.handleBankSelection} data={allBanks}/>
										{/* Android Picker */}
										{/* <Picker
											selectedValue={this.state.selectedBank} 
											style={{height: 50, width: '100%'}} onValueChange={(itemValue, itemIndex) => this.setState({selectedBank: itemValue})}>
											<Picker.Item disabled={true} label="Select Bank" value="null"/>
											{(allBanks && allBanks.length > 0) ? allBanks.map((item, index) => (
												<Picker.Item key={index} label={item.name} value={item} />
											)) : null}
										</Picker> */}
									</View>
									{(this.state.selectedBank && this.state.selectedBank.name == 'Other') ? 
										<View style={Styles.formField}>
										<View style={{flexDirection: 'row'}}>
											<Text style={{fontWeight: 'bold'}}>Other Bank Name</Text>
											<Text style={{color: 'red', marginLeft: 5}}>*</Text>
										</View>
										<TextInput onChangeText={(beneficiary_other_bank_name) => this.setState({beneficiary_other_bank_name: beneficiary_other_bank_name})} style={Styles.formInput}/>
											</View>
										:
										null}
									<View style={Styles.formField}>
										<View style={{flexDirection: 'row'}}>
											<Text style={{fontWeight: 'bold'}}>Account Number</Text>
											<Text style={{color: 'red', marginLeft: 5}}>*</Text>
										</View>
										<TextInput onChangeText={(bank_account_number) => this.setState({bank_account_number: bank_account_number})} style={Styles.formInput}/>
									</View>
									<View style={Styles.formField}>
										<View style={{flexDirection: 'row'}}>
											<Text style={{fontWeight: 'bold'}}>Confirm Account Number</Text>
											<Text style={{color: 'red', marginLeft: 5}}>*</Text>
										</View>
										<TextInput onChangeText={(c_bank_account_number) => this.setState({c_bank_account_number: c_bank_account_number})} style={Styles.formInput}/>
									</View>
								</View>
								:
								<View style={Styles.formField}>
									<View style={{flexDirection: 'row'}}><Text style={Styles.inputLabel}>Payout Location</Text><Text style={{color: 'red', marginLeft: 5}}>*</Text></View>
									{/* IOs Picker */}
									<ProviderPicker selectedValue={this.handleSelectedProvider} onChange={this.handleProviderSelection} data={allProviders}/>
									{/* Android Picker */}
									{/* <Picker selectedValue={this.state.selectedProvider} style={{height: 50, width: '100%'}} onValueChange={(itemValue, itemIndex) => this.setState({selectedProvider: itemValue})}>
										<Picker.Item disabled={true} label="Select Provider" value="null"/>
										{(allProviders !== null && allProviders.length > 0) ? allProviders.map((item, index) => (
											<Picker.Item key={index} label={item.name} value={item} />
										)) : null} 
									</Picker> */}
								</View>
							}
							<View style={Styles.formField}>
								<View style={{flexDirection: 'row'}}><Text style={Styles.inputLabel}>Relationship</Text><Text style={{color: 'red', marginLeft: 5}}>*</Text></View>
								<TextInput onChangeText={(relation) => this.setState({relation: relation})} style={Styles.formInput}/>
							</View>
							<View style={{width: '100%', alignContent: 'center', marginTop: 20, marginBottom: 20}}>
							<TouchableOpacity style={{height: 40, width: '80%', justifyContent: 'center', alignItems: 'center', alignSelf: 'center', backgroundColor: Constant.themeColor, borderRadius: 10}} onPress={this.handleAddBeneficiary}>
								<Text style={{color: 'white'}}>Add this beneficiary</Text>
							</TouchableOpacity>
						</View>
						</View>
					</ScrollView>
				</KeyboardAvoidingView>
			</View>
			{(loading) ? 
			<View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignContent: 'center', justifyContent: 'center', backgroundColor: 'rgba(10,01,01,0.4)'}}>
				<ActivityIndicator size="large" color="red"/>
			</View>
			: null }
			</SafeAreaView>
		);
	}
}

const Styles = StyleSheet.create({
	addViewStyle: {
		alignItems:'center',
		marginTop: 5,
		marginBottom: 5,
	},
	addTextStyle: {
		fontSize: 30,
		color: 'grey',
		fontWeight: 'bold'
	},
	
	formField: {
		paddingLeft: 10,
		paddingRight: 10,
		marginTop: 5,
	},
	formInput: {
		borderWidth: 1,
		borderColor: Constant.borderGrey,
		marginTop: 5,
		height: 40,
		borderRadius: 10,
	},
	padding5: {
		padding:5
	},
	iconStyle:{ height: 30, width: 30},
	box:{
		backgroundColor: 'red',
		padding:40
	},
	form:{
		width:'90%',
		borderWidth: 1,
		borderColor: Constant.borderGrey,
		alignSelf:'center',
		backgroundColor:'white',
		margin: 10,
		shadowOffset: { width: 5, height: 3 },
		shadowColor: 'black',
		shadowOpacity: 0.4,
		elevation: 2,
		borderRadius: 10,
		paddingTop:10
	},
	inputLabel: {
		fontWeight: 'bold'
	}
});