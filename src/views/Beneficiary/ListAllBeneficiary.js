import React from 'react';
import {
  Alert,
  TextInput,
  Image,
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
  TouchableOpacity
} from 'react-native';
import CommonHeader from '../../components/CommonHeader';
import Network from '../../services/NetworkRequest';
import Storage from '../../services/Storage';
import Constant from '../../services/Constants';

export default class BeneficiaryList extends React.Component {
  state = {
    list: [],
    loading: true,
    reloadStatus: true,
    beneficiaries: []
  };
  handleSearchSubmit = () => {};
  createList = data => {
    console.log(data);
    return (
      <View style={Styles.item}>
        <View style={Styles.userView}>
          <Text style={Styles.label}>FULL NAME</Text>
          <Text style={Styles.label_data}>
            {data.first_name} {data.last_name}
          </Text>
        </View>
        <View style={Styles.userView}>
          <Text style={Styles.label}>EMAIL-ID</Text>
          <Text style={Styles.label_data}>{data.email}</Text>
        </View>
        <View style={Styles.userView}>
          <Text style={Styles.label}>CURRENCY</Text>
          <Text style={Styles.label_data}>{data.currency}</Text>
        </View>
        <View style={Styles.userView}>
          <Text style={Styles.label}>Transaction Type</Text>
          <Text style={Styles.label_data}>{data.transaction_type}</Text>
        </View>
        {(data.transaction_type == 'CASH') ? 
        <View style={Styles.userView}>
          <Text style={Styles.label}>PHONE NUMBER</Text>
          <Text style={Styles.label_data}>{data.bank_account_number}</Text>
        </View>
        :
        <View style={Styles.userView}>
          <Text style={Styles.label}>ACCOUNT NUMBER</Text>
          <Text style={Styles.label_data}>{data.bank_account_number}</Text>
        </View>}
      </View>
    );
  };
  getBeneficiaryList = () => {
    let that = this;
    Network.beneficiaryList().then(
      resp => {
        console.log(resp);
        if (resp.success) {
          that.setState({
            beneficiaries: resp.data.beneficiary,
            loading: false,
          });
        } else {
          that.setState({
            beneficiaries: [],
            loading: false
          }, () => {
            Alert.alert('Error', resp.errors.error);
          });
        }
      }).catch(error => {
        that.setState({
          beneficiaries: [],
          loading: false
        }, () => {
          Alert.alert('Error', error.message);
        });
        console.log(error);
    });
  };
  componentDidMount() {
    Storage.getData('userData').then(resp => {
      console.log(resp);
			if(!resp.email_verified) {
				Alert.alert('Error', 'Please verify email.');
        this.props.navigation.navigate('Dashboard');
        return;
			} else if(!resp.mobile_number_verified) {
        this.props.navigation.navigate('IdCheck');
        return;
      } else if(resp.aasm_state != "accepted" && resp.aasm_state != "verified") {
        this.props.navigation.navigate('IdCheck');
        return;
      } else {
        this.getBeneficiaryList();
      }
		}, err => {
			console.log(err);
		});
  }
  render() {
    let { beneficiaries, loading, reloadStatus } = this.state;
    if (this.props.navigation.getParam('reload') && reloadStatus) {
      this.setState({
        reloadStatus: false
      }, () => {
        this.getBeneficiaryList();
      });
    }
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CommonHeader title="Beneficiary" {...this.props}/>
        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0)' }}>
          <View
            style={[Styles.topImage,{borderBottomWidth: 1, borderBottomColor: '#888881'}]}>
            <Image style={{ height: 100, width: 100, alignSelf: 'center' }} source={require('../../assets/beneficiary.png')}/>
            <RightNav setParam={() => this.setState({reloadStatus: true})} {...this.props} />
          </View>
          {loading ? (
            <View style={{flex: 1, alignContent: 'center', justifyContent: 'center'}}>
              <ActivityIndicator size="large" color="red" />
            </View>) 
            : beneficiaries.length > 0 ? (
            <FlatList data={beneficiaries} renderItem={({ item }) => this.createList(item)} keyExtractor={item => 'view' + item.id}/>) 
            : (<View style={{alignContent: 'center', flex: 1, justifyContent: 'center'}}>
              <Text style={{ fontSize: 18, textAlign: 'center' }}>
                No Beneficiary Added. Add new beneficiary and start sending
                money
              </Text>
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}

class RightNav extends React.Component {
  handleAddNewBene = () => {
    this.props.setParam();
    this.props.navigation.navigate('Add');
  }
  render() {
    return (
      <View style={{ width: '50%', alignSelf: 'center' }}>
        <TouchableOpacity
          style={Styles.navBtn}
          onPress={this.handleAddNewBene}>
          <Text style={{ color: 'white', textAlign: 'center' }}>+ Add New Beneficiary</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  item: {
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    margin: 10,
    backgroundColor: 'white',
    shadowOffset: { width: 5, height: 2 },
    shadowColor: 'black',
    shadowOpacity: 0.4,
    elevation: 5,
    width: '90%',
    alignSelf: 'center',
    borderRadius: 10
  },
  label: {
    flex: 0.5,
    fontWeight: 'bold'
  },
  label_data: {
    flex: 0.5,
    alignContent: 'flex-end'
  },
  userView: {
    flex: 1,
    margin: 5,
    flexDirection: 'row'
  },
  navBtn: {
    padding: 10,
    backgroundColor: Constant.themeColor,
    borderRadius: 10
  },
  searchBox: {
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 10,
    paddingLeft: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    shadowOffset: {
      height: 18,
      width: 20
    },
    elevation: 5,
    shadowRadius: 20,
    shadowColor: 'black'
  },
  topImage: {
    paddingTop: 10,
    paddingBottom: 10
  }
});
