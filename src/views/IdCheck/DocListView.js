import React from 'react';
import { ScrollView, View, Text, Image, StyleSheet } from 'react-native';

export default class DocListView extends React.Component {
  state = {
    list: this.props.data,
    loading: true
  };
  handleImageLoad = () => {
    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.list) {
      return (
        <ScrollView>
          <View style={Styles.form}>
            <View style={Styles.Holder}>
              <Text style={Styles.Heading}>Identity Documents</Text>
              {this.state.list.identity_documents.map((item, index) => (
                <View key={index}>
                  <Text style={Styles.docType}>{item.document_name}</Text>
                  <Image
                    style={Styles.docImage}
                    source={{ uri: item.public_url }}
                    onLoad={this.handleImageLoad}
                  />
                  <Text style={Styles.docName}>{item.face_type}</Text>
                </View>
              ))}
            </View>
            <View style={Styles.Holder}>
              <Text style={Styles.Heading}>KYC Documents</Text>
              {this.state.list.kyc_documents.map((item, index) => (
                <View key={index}>
                  <Text style={Styles.docType}>{item.document_name}</Text>
                  <Image
                    style={Styles.docImage}
                    source={{ uri: item.public_url }}
                    onLoad={this.handleImageLoad}
                  />
                  <Text style={Styles.docName}>{item.face_type}</Text>
                </View>
              ))}
            </View>
          </View>
        </ScrollView>
      );
    } else {
      return null;
    }
  }
}

const Styles = StyleSheet.create({
  Heading: {
    padding: 10,
    backgroundColor: '#48baf8',
    color: 'white',
    textAlign: 'center',
    marginBottom: 20
  },
  docName: {
    position: 'absolute',
    top: 10,
    right: 20,
    elevation: 5,
    backgroundColor: 'white',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 10,
    fontWeight: 'bold'
  },
  docType: {
    position: 'absolute',
    bottom: 10,
    left: 20,
    elevation: 5,
    backgroundColor: 'white',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 10,
    fontWeight: 'bold'
  },
  docImage: {
    height: 300,
    width: '80%',
    alignSelf: 'center'
  },
  Holder: {
    margin: 10,
    borderWidth: 1,
    borderColor: '#888'
  },
  form: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: 'white',
    margin: 10,
    shadowOffset: { width: 10, height: 10 },
    shadowColor: 'black',
    shadowOpacity: 1.0,
    elevation: 5,
    borderRadius: 10
  }
});
