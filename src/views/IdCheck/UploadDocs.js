import React from 'react';
import {ActivityIndicator, Button, View, Text, Alert, SafeAreaView, ScrollView, Picker, Image, PermissionsAndroid, TouchableOpacity, TextInput, Switch, StyleSheet, Platform} from "react-native";
import CommonHeader from "../../components/CommonHeader";
import ImagePicker from 'react-native-image-picker';
//import DateTimePicker from '@react-native-community/datetimepicker';
import Network from "../../services/NetworkRequest";
import Storage from "../../services/Storage";
import Constant from "../../services/Constants";
import DocListView from './DocListView';
import { is } from '@babel/types';
import DateTimePicker from "react-native-modal-datetime-picker";
import {PERMISSIONS, request} from 'react-native-permissions';


async function requestCameraPermission() {
	try {
		const granted = await PermissionsAndroid.request(
			PermissionsAndroid.PERMISSIONS.CAMERA,
			{
				title: 'App require permission',
				message:
					'Application requires camera permission to click, select and upload document',
				buttonNegative: 'Cancel',
				buttonPositive: 'OK',
			},
		);
		if (granted === PermissionsAndroid.RESULTS.GRANTED) {
			console.log('You can use the camera');
		} else {
			console.log('Camera permission denied');
		}
	} catch (err) {
		console.warn(err);
	}
}
export default class UploadDocs extends React.Component {
	state = {
		docType: false,
		frontImageLoader: false,
		backImageLoader: false,
		addressImageLoader: false,
		UserDocuments: null,
		selectedBackImage: null,
		expiryDate: new Date('2021-01-01T14:42:42'),
		issueDate: new Date('2018-01-01T14:42:42'),
		isDateTimePickerVisible: false,
		isExpirayDateTapped: false,
		show2: false,
		isDateValid: false,
		kycDocList: null,
		identityDocList: null,
		loading: false,
	};

    showDateTimePicker = () => {
		this.setState({ isDateTimePickerVisible: true });
	  };
	
	  hideDateTimePicker = () => {
		this.setState({ isDateTimePickerVisible: false });
	  };
	
	  handleExpiryDatePicked =  date => {
		expiryDate = date || this.state.expiryDate;
		console.log("A date has been picked: ", date);
		this.setState({ 
			expiryDate,
		});
		this.hideDateTimePicker();
	  };

	  handleIssueDatePicked =  date => {
		issueDate = date || this.state.issueDate;
		console.log("A date has been picked: ", date);
		this.setState({ 
			issueDate,
		});
		this.hideDateTimePicker();
	  };

	setExpiryDate = (event, date) => {
		expiryDate = date || this.state.expiryDate;
		this.setState({
		  show1: Platform.OS === 'ios' ? false : true,
		  expiryDate,
		});
	}
	setIssueDate = (event, date) => {
		issueDate = date || this.state.issueDate;
		this.setState({
		  show2: Platform.OS === 'ios' ? false : true,
		  issueDate,
		});
	}
	handleShowIssueDate = () => {
		this.setState({
			isDateTimePickerVisible: true,
			isExpirayDateTapped: false
		});
		// <DateTimePicker
		// isVisible={this.state.isDateTimePickerVisible}
		// onConfirm={this.handleIssueDatePicked}
		// onCancel={this.hideDateTimePicker}
		// />
	}
	handleShowExpiryDate = () => {
		this.setState({
			isDateTimePickerVisible: true,
			isExpirayDateTapped: true
		});

		// <DateTimePicker
		// isVisible={this.state.isDateTimePickerVisible}
		// onConfirm={this.handleExpiryDatePicked}
		// onCancel={this.hideDateTimePicker}
		// />
	}

	handleFrontImage = () => {
		const options = {
			noData: true,
		}
		ImagePicker.showImagePicker(options, (response) => {
			console.log('Response = ', response);
			if (response.didCancel) {
				console.log('User cancelled image picker');
			} else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
			} else {
				const source = { uri: response.uri };
				this.setState({
					selectedFrontImage: source,
				}, () => this.uploadImageDocument('identityDoc1'));
			}
		});
	}
	handleBackImage = () => {
		const options = {
			noData: true,
		}
		ImagePicker.showImagePicker(options, (response) => {
			console.log('Response = ', response);
			if (response.didCancel) {
				console.log('User cancelled image picker');
			} else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
			} else {
				const source = { uri: response.uri };		
				this.setState({
					selectedBackImage: source,
				}, () => this.uploadImageDocument('identityDoc2'));
			}
		});
	}
	handleAddressProofImage = () => {
		const options = {
			noData: true,
		}
		ImagePicker.showImagePicker(options, (response) => {
			console.log('Response = ', response);
			if (response.didCancel) {
				console.log('User cancelled image picker');
			} else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
			} else {
				const source = { uri: response.uri };
				console.log(source);		
				this.setState({
					selectedAddressProofImage: source,
				}, () => this.uploadImageDocument('kycDocument'));
			}
		});
	}
	uploadImageDocument = (type) => {
		let { selectedAddressProofImage, selectedBackImage, selectedFrontImage } = this.state;

		if(type == 'kycDocument') {
			this.setState({
				addressImageLoader: true
			});
			Network.uploadDocument(selectedAddressProofImage).then(resp => {
				console.log('uploadDocument/kycDocument--->', resp);
				if(resp.success) {
					this.setState({ public_url_kyc: resp.public_url, addressImageLoader: false });
				} else {
					Alert.alert('Error', 'Unable to upload image.');
				}
			}, err => {
				console.log(err);
				Alert.alert('Error', 'Server exception');
			});
		}
		if(type == 'identityDoc2') {
			this.setState({
				backImageLoader: true
			});
			Network.uploadDocument(selectedBackImage).then(resp => {
				console.log('uploadDocument/identityDoc2--->', resp);
				if(resp.success) {
					this.setState({ public_url_identity_1: resp.public_url, backImageLoader: false });
				} else {
					Alert.alert('Error', 'Unable to upload image.');
				}
			}, err => {
				console.log(err);
				Alert.alert('Error', 'Server exception');
			});
		}
		if(type == 'identityDoc1') {
			this.setState({
				frontImageLoader: true
			});
			Network.uploadDocument(selectedFrontImage).then(resp => {
				console.log('uploadDocument/identityDoc1--->', resp);
				if(resp.success) {
					this.setState({ public_url_identity_2: resp.public_url, frontImageLoader: false });
				} else {
					Alert.alert('Error', 'Unable to upload image');
				}
			}, err => {
				console.log(err);
				Alert.alert('Error', 'Server exception');
			});
		}
	}
	
	getIdentityDocumentList = () => {
		this.setState({
			loading: true
		});
		Network.getDocumentType('Identity').then(resp => {
			console.log(resp);
			this.setState({ 
				identityDocList: resp.data.identity_documents,
				loading: false
			});
		}, err => {
			this.setState({
				loading: false
			});
			console.log(err);
		});
	}
	getKycDocumentList = () => {
		Network.getDocumentType('KYC').then(resp => {
			console.log(resp);
			this.setState({
				loading: false,
				kycDocList: resp.data.kyc_documents
			});
		}, err => {
			this.setState({
				loading: false
			});
			console.log(err);
		});
	}
	getInitalDocuments = async() => {
		this.setState({
			loading: true
		});
		await Network.getUserDocuments().then(resp => {
			console.log(resp);
			this.setState({
				loading: false,
				UserDocuments: resp.user_documents
			});
		}, err => {
			this.setState({
				loading: false
			});
			console.log(err);
		});
	}
	componentDidMount() {

		request(
			Platform.select({
			  android: PERMISSIONS.ANDROID.CAMERA,
			  ios: PERMISSIONS.IOS.CAMERA,
			}),
		  );


		this.getInitalDocuments();
		if(this.state.UserDocuments == null) {
			requestCameraPermission();
			this.getIdentityDocumentList();
			this.getKycDocumentList();
		}
	}
	separateMetaData = (string) => {
		let metaData;
		let docs = this.state.identityDocList;
		for(var t = 0; t < docs.length; t++) {
			if(docs[t]['name'] == string) {
				metaData = docs[t]['metacontents'];
			} 
			if(docs[t]['name'] == string) {
				metaData = docs[t]['metacontents'];
			}
		}
		return metaData;
	}

	handleSubmit = () => {
		let postDict = this.state;
		console.log(postDict);
		if(this.state.selectedFrontImage == null && this.state.selectedAddressProofImage == null && this.state.selectedBackImage == null) {
			return Alert.alert('Error', 'Select All Documents Before Upload');
		}
		if(postDict.frontImageLoader || postDict.addressImageLoader && (postDict.docType ? postDict.backImageLoader : true)) {
			Alert.alert('Wait', 'Please wait while we are uploading your documents');
			return;
		}
		if(this.state.docType) {
			postDict.metaData = this.separateMetaData('Driving License');
		} else {
			postDict.metaData = this.separateMetaData('Passport');
		}
		this.setState({loading:true});
		console.log(postDict);
		Network.submitDocuments(postDict).then(resp => {
			this.setState({loading:false})
			console.log(resp);
			if(resp.success) {
				this.getInitalDocuments();
				Alert.alert('Success', 'Documents have been uploaded');
			} else {
				Alert.alert('Error', resp.errors.error);
			}
		}, err => {
			this.setState({loading:false})
			Alert.alert('Error', 'Some Exception');
			console.log(err);
		}).catch(err => {
			this.setState({loading:false})
			console.log(err);
		});
	}

	render() {
		const { loading } = this.state;
		const { issueDate, expiryDate, selectedAddressProofImage, identityDocList, UserDocuments, frontImageLoader, backImageLoader, addressImageLoader } = this.state;
		

		return(
			<SafeAreaView style={{flex: 1}}>
				<CommonHeader {...this.props} title="ID Check" />
					<View style={{flex: 1}}>
						<View style={[Styles.topImage,{borderBottomWidth: 1, borderBottomColor: '#888881'}]}>
							<Image style={{ height: 100, width: 100, alignSelf: 'center', marginTop: 10 }} source={require('../../assets/beneficiary.png')}/>
							<View style={Styles.uploadDocsViewStyle}>
								<Text style={Styles.uploadDocsTextStyle}>Upload Documents</Text>
							</View>
						</View>
					{(loading) ? 
						<View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignContent: 'center', justifyContent: 'center', backgroundColor: 'rgba(10,01,01,0.4)'}}>
					    <ActivityIndicator size="large" color="red"/>
						</View>
						:
						(UserDocuments == null || UserDocuments == undefined) ? 
						<ScrollView>
							<View style={Styles.form}>	
								<View style={Styles.IdentityDocs}>
								<Text style={Styles.heading}>Identity Documents</Text>
								<View style={Styles.contentHolder}>
									<View style={{flexDirection: 'row'}}>
										<Switch value={this.state.docType} onChange={(docType) => this.setState({docType: !this.state.docType})}></Switch>
										{(this.state.docType) ? <Text style={Styles.DocType}>Driving License</Text> : <Text style={Styles.DocType}>Passport</Text>}
									</View>
									<View style={Styles.formField}>
										<Text>Issuer Country</Text>
										<TextInput onChangeText={(issuer) => this.setState({
											issuer_country: issuer
										})} style={Styles.formInput}/>
									</View>
									<View style={Styles.formField}>
										<Text>Issued By</Text>
										<TextInput onChangeText={(issuedby) => this.setState({issuedby: issuedby})} style={Styles.formInput}/>
									</View>
									<View style={Styles.formField}>
										<Text>ID Number</Text>
										<TextInput onChangeText={(id_number) => this.setState({id_number: id_number})} style={Styles.formInput}/>
									</View>


									<View style={{flexDirection: 'row', paddingVertical: 10}}>

										<View style={Styles.formField}>
											<Text style={{fontWeight: 'bold'}}>Expiration Date</Text>
											<View style={{flexDirection: 'row'}}>
												<TouchableOpacity onPress={this.handleShowExpiryDate}>
													<Image style={{height: 30, width: 30}} source={require('../../assets/calender-logo.png')}/>
												</TouchableOpacity>
												<Text style={{padding: 2}}>{expiryDate.toDateString()}</Text>
												
												{/* {show1 && <DateTimePicker value={expiryDate} is24Hour={true} display='' onChange={this.setExpiryDate}/>} */}
											</View>
                                                 
										</View>

										<View style={Styles.formField}>
											<Text style={{fontWeight: 'bold'}}>Date of Issue</Text>
											<View style={{flexDirection: 'row'}}>
												<TouchableOpacity onPress={this.handleShowIssueDate}>
													<Image style={{height: 30, width: 30}} source={require('../../assets/calender-logo.png')}/>
												</TouchableOpacity>
												<Text style={{padding: 2}}>{issueDate.toDateString()}</Text>
												{/* {show2 && <DateTimePicker value={issueDate} is24Hour={true} display="default" onChange={this.setIssueDate}/>} */}
											</View>
										</View>
									</View>

									<View style={Styles.formField} onLayout={(event) => {
										var {x, y, width, height} = event.nativeEvent.layout;
										console.log(x);
										console.log(y);
										console.log(width);
										console.log(height);}}>
										{(this.state.selectedFrontImage == null) ?
											<TouchableOpacity onPress={this.handleFrontImage} style={{width: '100%', height: 200, alignContent: 'center', justifyContent: 'center', alignItems: 'center', borderRadius: 20, flexDirection: 'row',borderWidth: 2,
											borderColor: Constant.themeColor,borderStyle:'dotted'}}>
												<Text>+ </Text>
												<Text>SELECT FRONT SIDE</Text>
											</TouchableOpacity>
										: 
										null}
										<View>
											{(frontImageLoader) ? 
												<ActivityIndicator color="red"/>
												:
												(this.state.selectedFrontImage != null) ?
												<View>
													<Text style={Styles.imageOverText}>FRONT SIDE</Text>
													<Image style={{height: 300, width: '100%'}} source={this.state.selectedFrontImage}/>
												</View>
													: null
											}
											</View>
									</View>
									<View style={Styles.formField}>
										{(this.state.docType && this.state.selectedBackImage == null) ?
											<TouchableOpacity onPress={this.handleBackImage} style={{width: '100%', height: 200, alignContent: 'center', justifyContent: 'center', alignItems: 'center', borderRadius: 20, flexDirection: 'row',borderWidth: 2,
											borderColor: Constant.themeColor,borderStyle:'dotted'}}>
												<Text>+ </Text>
												<Text>SELECT BACK SIDE</Text>
											</TouchableOpacity>
											: 
										null}
										{(this.state.docType && backImageLoader) ? 
										<ActivityIndicator color="red"/>
										:												
										(this.state.selectedBackImage != null) ?
											<View style={{position: 'relative'}}>
												<Text style={Styles.imageOverText}>BACK SIDE</Text>
												<Image style={{height: 300, width: '100%'}} source={this.state.selectedBackImage}/>
											</View>
										: null}
									</View>
								</View>
								</View>
							</View>

							<View style={Styles.form}>	
								<View style={Styles.AddressProof}>
									<Text style={Styles.heading}>Address Proof</Text>
									<View>
										<View style={{padding: 10}}>
											<Picker style={{backgroundColor: '#70707020', borderRadius: 10}} selectedValue={this.state.selectedAddressProof} onValueChange={(addProof, itemIndex) => this.setState({
												selectedAddressProof: addProof
											})}>
												<Picker.Item label="Select Doc Type" value="default"/>
												{(this.state.kycDocList !== null) ? 
													this.state.kycDocList.map((item, index) => (
														<Picker.Item key={index} label={item.name} value={item.id}/>
													))
												:
												null}
											</Picker>
										</View>
										<View style={{padding: 20}}>
											{(selectedAddressProofImage == null) ? 
												<TouchableOpacity onPress={this.handleAddressProofImage} style={{width: '100%', height: 200, alignContent: 'center', justifyContent: 'center', alignItems: 'center', borderRadius: 20, flexDirection: 'row',borderWidth: 2,
												borderColor: Constant.themeColor,borderStyle:'dotted'}}>
													<Text style={{marginRight: 10}}>+</Text>
													<Text>SELECT DOCUMENT</Text>
												</TouchableOpacity>
												:
												null
											}
											<View>
											{(addressImageLoader) ? 
												<ActivityIndicator color="red"/>
												:
												(selectedAddressProofImage != null) ?
													<Image style={{height: 200, width: '100%'}} source={this.state.selectedAddressProofImage}/>
													: null
											}
											</View>
										</View>
									</View>
								</View>
								<View style={{margin: 10, padding: 10}}>
								<TouchableOpacity onPress={this.handleSubmit} style={Styles.buttonStyle}>
									<Text style={Styles.buttonTextStyle}>SUBMIT</Text>
								</TouchableOpacity>
							</View>
							</View>

						</ScrollView>
						:
						<DocListView data={UserDocuments}/>
					}

					{(this.state.isExpirayDateTapped) ? <DateTimePicker
							isVisible={this.state.isDateTimePickerVisible}
							onConfirm={this.handleExpiryDatePicked}
							onCancel={this.hideDateTimePicker}
							/> :  <DateTimePicker
						      isVisible={this.state.isDateTimePickerVisible}
							 onConfirm={this.handleIssueDatePicked}
							 onCancel={this.hideDateTimePicker}
							 />}
						

     			    {/* {show1 && <DateTimePicker value={expiryDate} is24Hour={true} display='default' onChange={this.setExpiryDate}/>}
					{show2 && <DateTimePicker value={issueDate} is24Hour={true} display="default"  onChange={this.setIssueDate}/>}
					 */}
				</View>
			</SafeAreaView>
		);
	}
}

const Styles = StyleSheet.create({
	uploadDocsViewStyle: {
		alignItems:'center',
		marginTop: 5,
		marginBottom: 5,
	},
	uploadDocsTextStyle: {
		fontSize: 30,
		color: 'grey',
		fontWeight: 'bold'
	},
	IdentityDocs: {
		margin: 5,
	},
	placeholder: {
		width: '100%', 
		height: 200,
		borderWidth: 1,
		padding: 10,
		borderColor: 'black',
		borderStyle: 'dashed', 
		borderRadius: 5,
		// backgroundColor: '#ccc', 
		alignContent: 'center', 
		justifyContent: 'center', 
		alignItems: 'center', 
		borderRadius: 20, 
		flexDirection: 'row'
	},
	buttonStyle: {
		width: '100%',
		marginTop: 10,
		paddingTop: 10,
		paddingLeft: 20,
		paddingRight: 20,
		paddingBottom: 15,
		backgroundColor: Constant.themeColor,
		borderRadius: 20,
		borderWidth: 1,
		borderColor: "#fff"
	},
	formField: {
		paddingLeft: 10,
		paddingRight: 10,
		marginTop: 5,
	},
	formInput: {
		borderWidth: 1,
		marginTop: 5,
		borderRadius: 10,
	},
	imageOverText: {
		position: 'absolute', 
		top: 10, 
		color: 'white', 
		elevation: 5, 
		alignSelf: 'center', 
		padding: 10
	},
	DocType: {
		padding: 5
	},
	contentHolder: {
		padding: 10
	},
	heading: {
		padding: 10,
		backgroundColor: Constant.themeColor,
		color: 'white',
	},
	AddressProof: {
		margin: 5
	},
	buttonTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "800"
  },
  form:{
	width:'90%',
	alignSelf:'center',
	backgroundColor:'white',
margin: 10,
shadowOffset: { width: 10, height: 10 },
shadowColor: 'black',
shadowOpacity: 1.0,
elevation: 5,
borderRadius: 10,
paddingTop:10
}
})
