import React from 'react';
import {
	Modal,
	View,
	ActivityIndicator,
	DatePickerIOS,
	Platform,
	Image,
	Text,
	SafeAreaView,
	KeyboardAvoidingView,
	ScrollView,
	TouchableOpacity,
	Picker,
	TextInput,
	StyleSheet,
	Alert
} from 'react-native';

import CommonHeader from "../../components/CommonHeader";
import DateTimePicker from '@react-native-community/datetimepicker';
import Network from "../../services/NetworkRequest";
import Storage from "../../services/Storage";
import Constant from "../../services/Constants";
import OccupationPicker from "../../components/OccupationPicker";
import CountryPicker from "../../components/CountryPicker";
import StatePicker from "../../components/StatePicker";

export default class IdCheckScreen extends React.Component {
	state = {
		isMobileNumberVerified: false,
		modalView: false,
		occupationsList: null,
		oldData: {},
		occupation: null,
		selectedOccupation: null,
		date_of_birth: new Date('2019-01-01'),
		showCalender: false,
		selectedState: "",
		selectedCountry: "",
		first_name: '',
		last_name: '',
		mobile_number: '',
		email: '',
		statesList: null,
		address: '',
		city: '',
		state_id: '',
		countryList: [],
		country: 'United Kingdom',
		zipcode: '',
		loading: false,
	};

	getAllOccupations = () => {
		this.setState({ loading: true });
		Network.getOccupations().then(resp => {
			this.setState({ loading: false });
			if (resp.success) {
				this.setState({ occupationsList: resp.data.occupations });
			} else {
				Alert.alert('Error Occured');
			}
		}, err => {
			this.setState({ loading: false });
			console.log(err);
		});
	}

	// getAllState = () => {
	// 	this.setState({ loading: true });
	// 	Network.getStates().then(resp => {
	// 		this.setState({ loading: false })
	// 		if (resp.success) {
	// 			this.setState({ statesList: resp.data.states });
	// 		} else {
	// 			Alert.alert('Error:', resp.message);
	// 		}
	// 	}, err => {
	// 		this.setState({ loading: false });
	// 		console.log(err);
	// 	});
	// }

	setOldData = () => {
		Storage.getData('userData').then(resp => {
			console.log(resp);
			var oldData = resp;
			// oldData.selectedOccupation = resp.occupation.id;
			let dob, occ;
			if(resp.occupation == null) {
				occ = '';
			} else {
				occ = resp.occupation.id
			}
			if(resp.birth_date != '') {
				dob = new Date(resp.birth_date);
			} else {
				dob = new Date();
			}
			this.setState({
				oldData: oldData, 
				email: resp.email, 
				mobile_number: resp.mobile_number,
				first_name: resp.first_name,
				last_name: resp.last_name,
				address: resp.address,
				city: resp.city,
				selectedCountry: resp.country, 
				zipcode: resp.zipcode,
				selectedState: resp.state,
				selectedOccupation: occ,
				date_of_birth: dob
			}, () => {
				console.log(this.state);
			});
		}, err => {
			console.log('Error Here');
			console.log(err);
		});
	}

	componentDidMount() {
		Storage.getData('userData').then(resp => {
			console.log(resp);
			if(!resp.email_verified) {
				Alert.alert('Error', 'Please verify email.');
				this.props.navigation.navigate('Dashboard');
				return;
			} else {
				this.getAllOccupations();
				this.getCountries();
				// this.getAllState();
				this.setOldData();
			}
			
		}, err => {
			console.log(err);
		});
	}

	handleAssmStateChange = () => {
		if(resp.aasm_state == "onboarded") {
			Network.updateUserDetails({aasm_state: "accepted"}).then(resp => {
				console.log(resp);
				Storage.setData('userData', resp.data.user).then(resp => {
					console.log('updateUserDetails-->', resp);
					this.setOldData();
				}, err => {
					console.log(err);
				});
			}, err => {
				console.log(err);
			});
		}
	}

	handleShowDob = () => { this.setState({ showCalender: true }); }

	dob = (event, date) => {
		date_of_birth = date || this.state.date_of_birth;
		this.setState({ showCalender: Platform.OS === 'ios' ? true : false, date_of_birth });
	}
	dob2 = (event, date) => {
		console.log(event);
		date_of_birth = new Date(event) || this.state.date_of_birth;
		this.setState({ showCalender: Platform.OS === 'ios' ? true : false, date_of_birth });
	}

	handleSubmit = () => {
		let { first_name, last_name, mobile_number, selectedOccupation, address, city,
			selectedState, zipcode, country } = this.state;
		if (first_name == '' || last_name == '' || mobile_number == '' || selectedOccupation == '' || address == '' || city == '' || selectedState == null || zipcode == '' || country == '') {
			console.log("state---->>>", this.state)
			Alert.alert("Please fill all the fields");
		}
		else {
			this.setState({ loading: true });
			let postDict = this.state;
			Network.updateUserDetails(postDict).then(resp => {
				console.log('updateUserDetails---->', resp);
				this.setState({ 
					loading: false
				});
				if (resp.success) {
					this.handleAssmStateChange();
					console.log('updateUserDetails after sucess---->', resp);
					Storage.setData('userData', resp.data.user).then(resp => {
						console.log('updateUserDetails-->', resp);
						Alert.alert('Success', 'Profile Updated');
						this.setOldData();
					}, err => {
						Alert.alert('Error', resp.errors.error);
						console.log(err);
					});
				} else {
					Alert.alert('Error', resp.errors.error);
				}
			}, err => {
				this.setState({ loading: false });
				Alert.alert('Error', 'Something went wrong, Please try again later');
				console.log(err);
			}).catch(err => {
				this.setState({ loading: false });
				Alert.alert('Error', 'Something went wrong, Please try again later');
				console.log(err);
			})
		 }
	}


	getOtpFromServer = () => {
		let that = this;
		console.log("mobile_number---> ", that.state.mobile_number);
		if (that.state.mobile_number == '') {
			that.setState({ loading: false });
			Alert.alert("Caution", 'Mobile Number is Empty');
			return;
		}
		that.setState({ loading: true });
		Network.getOtp(that.state.mobile_number, "Register").then(resp => {
			console.log(resp);
			if (resp.success) {
				that.setState({
					modalView: true,
					loading: false,
					otpData: resp.data.verification_code
				});
			} else {
				that.setState({ modalView: false, loading: false });
				Alert.alert("Error", resp.errors.error);
			}
		}, err => {
			that.setState({ modalView: false, loading: false });
			Alert.alert("Error", "Something went wrong, Please try again later");
			console.log(err);
		}).catch(error => {
			that.setState({ modalView: false, loading: false });
			Alert.alert("Error", "Something went wrong, Please try again later");
			console.log(error);
		})
	}

	getCountries = () => {
		Network.getAllCountries().then(resp => {
			console.log(resp);
			if(resp.success) {
				this.setState({
					countryList: resp.data.countries
				});
			} else {
				Alert.alert('Error', resp.errors.error);
			}
		}, err => {
			console.log(err);
		});
	}

	submitOtp = () => {
		this.setState({ loading: true });
		let that = this;
		if (this.state.otp != '') {
			let postDict = {
				otpID: this.state.otpData.id,
				otp: this.state.otp
			}
			Network.verfiyOtp(postDict).then(resp => {
				console.log("submitOtp ---> ", resp);
				if(resp.success) {
					Storage.setData('userData', resp.data.user).then(resp => {
						console.log('updateUserDetails-->', resp);
						that.setState({
							modalView: false,
							loading: false,
							isMobileNumberVerified: true,
						}, () => {
							//Alert.alert('Success', 'Profile Updated');
							that.setOldData();
						});
					}, err => {
						that.setState({
							modalView: false,
							loading: false,
							isMobileNumberVerified: false
						}, () => {
						//	Alert.alert('Error', 'Number verified. Please Login Again to see updated info');
						});
						console.log(err);
					});
				} else {
					that.setState({ loading: false, modalView: false, isMobileNumberVerified: false }, () => {
					//	Alert.alert("Error", resp.error);
					});
				}
			}, err => {
				that.setState({ loading: false, modalView: false, isMobileNumberVerified: false }, () => {
					//Alert.alert("Error", JSON.stringify(err));
				});
				console.log(err);
			});
		} else {
			that.setState({ loading: false, modalView: false, isMobileNumberVerified: false }, () => {
				Alert.alert('Error', 'OTP was empty. Try Again.');
			});
		}
	}

	selectedOccupationAction = (occ) => {
		this.setState({
			selectedOccupation: occ
		});
	}
	selectedCountryModal = (coun) => {
		console.log(coun);
		this.setState({
			selectedCountry: coun,
			statesList: coun.states || []
		});
	}
	selectedStateAction = (stat) => {
		this.setState({
			selectedState: stat
		});
	}
	render() {
		let { showCalender,
			loading,
			selectedState,
			statesList,
			selectedOccupation,
			occupationsList,
			date_of_birth,
			first_name,
			last_name,
			email,
			mobile_number,
			address,
			city,
			zipcode,
			countryList,
			oldData,
			selectedCountry,
			isMobileNumberVerified
		} = this.state;
		console.log(selectedCountry);
		const stringifyDate = (date_of_birth) ? date_of_birth.toDateString() : '';
		return (
			<SafeAreaView style={{flex: 1}}>
				<CommonHeader {...this.props} title="ID Check" /> 
				<View style={[Styles.topImage, {borderBottomWidth: 1,borderBottomColor: '#888881'}]}>
					<Image style={{ height: 100, width: 100, alignSelf: 'center' }} source={require('../../assets/beneficiary.png')}/>
					<View style={Styles.IDCheckViewStyle}>
						<Text style={Styles.IDCheckTextStyle} >Enter User Details</Text>
					</View>
				</View>
				<View style={{flex: 1, height: '100%'}}>
					<KeyboardAvoidingView enabled behavior="height" keyboardVerticalOffset={200} style={{flex: 1, height:'100%'}}>
						<ScrollView keyboardDismissMode="on-drag">
							<View style={Styles.form}>	
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>First Name</Text>
									<TextInput value={first_name} onChangeText={(first_name) => this.setState({ first_name: first_name })} style={Styles.formInput} />
								</View>
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>Last Name</Text>
									<TextInput
										value={last_name}
										onChangeText={(last_name) => this.setState({ last_name: last_name })}
										style={Styles.formInput} />
								</View>
								<View style={Styles.formField}>
								    <View style={{flexDirection:'row'}}>
										<Text style={Styles.labelStyle}>Phone</Text>
										{(isMobileNumberVerified) ? <Text style={Styles.labelStyle}> (Verified)</Text> 
										: <Text style={Styles.labelStyle}> (Not Verified)</Text>}
									</View>
									<TextInput keyboardType="number-pad" editable={oldData.mobile_number == ""} value={mobile_number} onChangeText={(mobile_number) => this.setState({ mobile_number: mobile_number })} style={Styles.formInput}/>
									{(oldData.mobile_number_verified) ? null :
									<View style={[Styles.formField, { marginTop: 20, marginBottom: 20 }]}>
										<TouchableOpacity onPress={this.getOtpFromServer} style={[Styles.submitBtn, { borderRadius: 10 }]}>
											<Text style={{color:'white'}}>Verify Mobile Number</Text>
										</TouchableOpacity>
									</View>
									}
								</View>
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>Email-ID</Text>
									<TextInput keyboardType="email-address" editable={oldData.email == ""} value={email} onChangeText={(email) => this.setState({ email: email })} style={Styles.formInput}/>
								</View>
								<Modal animationType="slide" transparent={true} visible={this.state.modalView}>
									<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.5)' }}>
										<View style={{ height: 300, width: '80%', backgroundColor: 'white', alignSelf: 'center', justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}>
											<TextInput style={[Styles.formInput, {width: '60%'}]} placeholder="Enter OTP" keyboardType="number-pad" onChangeText={(otp) => this.setState({ otp })} />
											<TouchableOpacity style={[Styles.SubmitButtonStyle, {maxWidth: 220}]} onPress={this.submitOtp}>
												<Text style={{ color: 'white', fontWeight: 'bold', textAlign: 'center' }}>SUBMIT OTP</Text>
											</TouchableOpacity>
											<TouchableOpacity style={[Styles.SubmitButtonStyle, {maxWidth: 220}]} onPress={() => this.setState({modalView: false, loading: false})}>
												<Text style={{ color: 'white', fontWeight: 'bold', textAlign: 'center' }}>CANCEL</Text>
											</TouchableOpacity>
										</View>
									</View>
								</Modal>
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>Select Date of Birth</Text>
									<View style={(Platform.OS == 'ios') ? {flexDirection: 'column', justifyContent: 'center'} : { flexDirection: 'row', padding: 10 }}>
										<TouchableOpacity
											onPress={this.handleShowDob}>
											<Image style={{ height: 30, width: 30 }}
												source={require('../../assets/calender-logo.png')} />
										</TouchableOpacity>
										{(Platform.OS != 'ios') ? 
										<Text style={{ padding: 2 }}>{stringifyDate}</Text>
										:null}
										{(showCalender && Platform.OS != 'ios') && <DateTimePicker value={date_of_birth} is24Hour={true} display="default" onChange={this.dob} />}
										{(Platform.OS === 'ios') ? 
										<DatePickerIOS mode="date" style={{flex: 1}} date={date_of_birth} is24Hour={true} onDateChange={this.dob2}/> : null
										}
									</View>
								</View>
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>Occupation</Text>
									{/* Ios Picker */}
									<OccupationPicker data={occupationsList} result={this.selectedOccupationAction}/>
							{/* Android Picker */}
								{/* <View style={(Platform.OS == 'ios') ? {flexDirection: 'column', justifyContent: 'center'} : { flexDirection: 'row', padding: 10 }}>
									<Picker
										selectedValue={selectedOccupation}
										style={{ height: 50, width: '100%' }}
										onValueChange={(itemValue, itemIndex) => this.setState({ selectedOccupation: itemValue })}>
										<Picker.Item label="Select Occupation" value="null" />
										{(occupationsList != null) ? occupationsList.map((item, index) => (
											<Picker.Item key={index} label={item.name} value={item.id} />
										))
											:
											null
										}
									</Picker>

								</View> */}

								</View>
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>Address</Text>
									<TextInput
										value={address}
										onChangeText={(address) => this.setState({ address: address })}
										style={Styles.formInput} />
								</View>
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>City</Text>
									<TextInput
										value={city}
										onChangeText={(city) => this.setState({ city: city })}
										style={Styles.formInput} />
								</View>
								{(typeof selectedCountry == "string" && selectedCountry != "") ?
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>Country</Text>
									<TextInput editable={false} value={selectedCountry} style={Styles.formInput} />
								</View>
								:
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>Country</Text>
									{/* Ios Picker */}
									<CountryPicker data={countryList} result={this.selectedCountryModal}/>
									{/* Android Picker */}
									{/* <Picker
										selectedValue={selectedCountry}
										style={{ height: 50, width: '100%' }}
										onValueChange={(itemValue, itemIndex) => this.setState({ selectedCountry: itemValue, statesList: itemValue.states })}>
										<Picker.Item label="Select Country" value="null" />
										{(countryList != null) ? countryList.map((item, index) => (
											<Picker.Item key={index} label={item.name} value={item} />
										))
											:
											null
										}
									</Picker> */}
								</View>
								}
								{(typeof selectedState == 'string' && selectedState != "") ? 
									<View style={Styles.formField}>
										<Text style={Styles.labelStyle}>State</Text>
										<TextInput editable={false} value={selectedState} style={Styles.formInput} />
									</View>
									: 
									<View style={Styles.formField}>
										<Text style={Styles.labelStyle}>State</Text>
										{/* Ios Picker */}
										<StatePicker data={(statesList ? statesList : [])} result={this.selectedStateAction}/>
										{/* Android Picker */}
										{/* <Picker selectedValue={selectedState} style={{ height: 50, width: '100%' }} onValueChange={(itemValue, itemIndex) => this.setState({ selectedState: itemValue })}>
											<Picker.Item label="Select State" value="null" />
											{(statesList != null) ? statesList.map((item, index) => (
												<Picker.Item key={index} label={item.name} value={item} />
											))
												:
												null
											}
										</Picker> */}
									</View>
								}
								<View style={Styles.formField}>
									<Text style={Styles.labelStyle}>Postal Code</Text>
									<TextInput value={zipcode} onChangeText={(zipcode) => this.setState({ zipcode: zipcode })} style={Styles.formInput} />
								</View>
								{/* <View style={Styles.formField}>
									<Text style={Styles.labelStyle}>Country</Text>
									<TextInput editable={false} defaultValue={country.name} value={country.name} onChangeText={(country) => this.setState({country: country})} style={Styles.formInput} />
								</View> */}
								<View style={[Styles.formField, { marginTop: 20, marginBottom: 40 }]}>
									<TouchableOpacity onPress={this.handleSubmit} style={[Styles.submitBtn, { borderRadius: 10 }]}>
										<Text style={Styles.buttonTextStyle}>SUBMIT</Text>
									</TouchableOpacity>
								</View>
							</View>
						</ScrollView>
					</KeyboardAvoidingView>
				</View>
				{(loading) ?
					<View
						style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignContent: 'center', justifyContent: 'center', backgroundColor: 'rgba(10,01,01,0.4)' }}>
						<ActivityIndicator size="large" color="red" />
					</View>
					:
					null
				}
			</SafeAreaView>
		);
	}
}

const Styles = StyleSheet.create({
	IDCheckViewStyle: {
		alignItems:'center',
		marginTop: 5,
		marginBottom: 5,
	},
	IDCheckTextStyle: {
		fontSize: 30,
		color: 'grey',
		fontWeight: 'bold'
	},
	formField: {
		paddingLeft: 10,
		paddingRight: 10,
		marginTop: 5,
	},
	labelStyle: {
		fontWeight: 'bold'
	},
	formInput: {
		borderWidth: 1,
		marginTop: 5,
		height: 40,
		borderRadius: 10,
		borderColor: Constant.borderGrey,
	},
	submitBtn: {
		width: 250,
		height: 40,
		backgroundColor: Constant.themeColor,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center'
	},
	SubmitButtonStyle: {
		width: '100%',
		marginTop: 10,
		paddingTop: 10,
		paddingLeft: 20,
		paddingRight: 20,
		paddingBottom: 15,
		backgroundColor: Constant.themeColor,
		borderRadius: 20,
		borderWidth: 1,
		borderColor: "#fff"
	},
	form:{
		padding: 10,
		borderWidth: 1,
		borderColor: 'rgba(0,0,0,0.4)',
		width:'90%',
		alignSelf:'center',
		backgroundColor:'white',
    margin: 10,
    shadowOffset: { width: 5, height: 5 },
    shadowColor: 'black',
    shadowOpacity: 0.6,
    elevation: 1,
		borderRadius: 10,
		paddingTop:10
	},
	buttonTextStyle:{
		color:'white'
	}
});