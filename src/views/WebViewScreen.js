import React from  'react';
import { View, Text, Linking,Alert } from 'react-native';
import { WebView } from 'react-native-webview';
import Constant from '../services/Constants';

export default class WebViewScreen extends React.Component {
    state = {
        orderDetails: null
    };
    componentDidMount() {
        let data = this.props.navigation.getParam('orderDetails');
        this.setState({
            orderDetails: data
        });
        Linking.addEventListener('url', this.handleOpenURL);

    }
    handleOpenURL = (url) => {
        console.log('data returned', url);
    }
    componentWillUnmount() {
        Linking.removeEventListener('url', this.handleOpenURL)
    }
    _onLoad(webViewState) {
        let url = webViewState.url.toString();
        console.log(url);
        let split_url = url.split('?code=');
        console.log(split_url);
        if(split_url.length == 2) {
            var orderCode = split_url[1];
            if(url == Constant.baseUrl+'transactions/success/'+this.state.orderDetails.transaction.id+'?code='+orderCode) {
                Alert.alert("Roltex", "Your transaction has been successful.");
                this.props.navigation.navigate('SendMoney')
            }
            if(url == Constant.baseUrl+'transactions/failure/'+this.state.orderDetails.transaction.id+'?code='+orderCode) {
                Alert.alert("Roltex", "Your transaction has failed.");
                this.props.navigation.navigate('SendMoney')
            }
            if(url == Constant.baseUrl+'transactions/cancel/'+this.state.orderDetails.transaction.id+'?code='+orderCode) {
                Alert.alert("Roltex", "Your transaction has been cancelled."); 
                this.props.navigation.navigate('SendMoney')
            }
        }
    }

    render() {
        let url = this.props.navigation.getParam('url');
        return(
            <WebView
              source={{uri: url}}
              onNavigationStateChange={this._onLoad.bind(this)}
            />
        );
    }
}