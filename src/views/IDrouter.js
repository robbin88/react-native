import React from 'react';
import { Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import BasicForm from './IdCheck/BasicForm.js';
import UploadDocs from './IdCheck/UploadDocs.js';
import Constants from '../services/Constants';

const BeneficiaryStack = createBottomTabNavigator(
  {
    Basic: {
      screen: BasicForm,
      navigationOptions: {
        tabBarLabel: 'Basic Details',
        tabBarIcon: ({ tintColor }) => (
          <Image
            style={{ height: 30, width: 30, padding: 5 }}
            source={require('../assets/form.png')}
          />
        )
      }
    },
    Upload: {
      screen: UploadDocs,
      navigationOptions: {
        tabBarLabel: 'Upload Docs',
        tabBarIcon: ({ tintColor }) => (
          <Image
            style={{ height: 30, width: 30, padding: 5 }}
            source={require('../assets/document.png')}
          />
        )
      }
    }
  },
  {
    tabBarOptions: {
      keyboardHidesTabBar: true,
      activeTintColor: '#e6e6e6',
      labelStyle: {
        fontSize: 14
      },
      style: {
        height: 70,
        backgroundColor: Constants.themeColor,
        elevation: 2,
        shadowOffset: {
          height: 20,
          width: 15
        },
        shadowColor: 'white'
      }
    }
  }
);

export default createAppContainer(BeneficiaryStack);
