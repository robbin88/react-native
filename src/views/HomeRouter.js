import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Dashboard from "../views/Dashboard/Dashboard";
import TransactionScreen from "../views/Dashboard/RecentTransactionsList";


const HomeRoutes = createStackNavigator({
    Home: {screen: Dashboard},
    Transactions: {screen: TransactionScreen},
  }, {
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
  });
  
  export default createAppContainer(HomeRoutes);