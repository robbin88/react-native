import React, { Component } from 'react';
import {
  Image,
  Alert,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  View,
  Text,
  FlatList,
  ActivityIndicator
} from 'react-native';
//import { ScrollView } from "react-native-gesture-handler";
// import { List, ListItem, SearchBar } from "react-native-elements";
import Network from "../../services/NetworkRequest";
import Storage from "../../services/Storage";
import Constant from "../../services/Constants";
import CommonHeader from "../../components/CommonHeader";
import { CustomButton } from "../../components/StylesButton";

class RecentTransactionsList extends Component {
  state = {
    data: [],
    page: 1,
    seed: 1,
    error: null,
    refreshing: false,
    transactionsList: null,
    loading: false,
    wasPaymentSuccessful: false
  };

  componentDidMount() {
    this.getRecentTransactions();
  }
  getRecentTransactions = () => {
    this.setState({ loading: true });
    Network.recentTransactions().then(
      resp => {
        console.log('getTransaction---->', resp);
        if (resp.success) {
          this.setState({
            data: resp.data.transaction,
            refreshing: false,
            loading: false
          });
        } else {
          this.setState({
            loading: false,
            refreshing: false,
            error: 'No Transactions Found'
          });
        }
      },
      err => {
        this.setState({
          err,
          loading: false,
          refreshing: false
        });
        console.log(err);
      }
    );
  };

  handleRefresh = () => {
    this.setState({ loading: true });
    this.setState(
      {
        refreshing: true
      },
      () => {
        this.getRecentTransactions();
      }
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%'
        }}
      />
    );
  };

  handleSendMoney = (url, data) => {
    console.log(data);
    console.log('payment url ---------->', url);
    if (url != null && url != undefined) {
      this.props.navigation.navigate('WebView', { url: url, orderDetails:  {transaction: data}});
    }
  };

  render() {
    let { loading, wasPaymentSuccessful } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{padding: 10, flexDirection: 'row',borderBottomWidth: 1, borderBottomColor: '#888', elevation: 1}}>
					<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
						<Image style={Styles.iconStyle} source={require('../../assets/back-icon.png')}/>
					</TouchableOpacity>
					<Text style={Styles.headerTitle}>Transactions List</Text>
				</View> 
        <View style={{ flex: 1 }}>
          <View style={[Styles.topImage,{borderBottomWidth: 1,borderBottomColor: '#888881'}]}>
            <Image style={{ height: 100, width: 100, alignSelf: 'center' }} source={require('../../assets/transactions.png')}/>
            <RightNav {...this.props} />
          </View>
          <ScrollView containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            <FlatList
              data={this.state.data}
              renderItem={({ item }) => (
                <View style={Styles.item}>
                  <View style={Styles.userView}>
                    <Text style={Styles.label}>ID</Text>
                    <Text style={Styles.label_data}>{item.id}</Text>
                  </View>
                  <View style={Styles.userView}>
                    <Text style={Styles.label}>Benefieciary</Text>
                    <Text style={Styles.label_data}>
                      {item.beneficiary_first_name} {item.beneficiary_last_name}
                    </Text>
                  </View>
                  <View style={Styles.userView}>
                    <Text style={Styles.label}>Country</Text>
                    <Text style={Styles.label_data}>{item.payout_country}</Text>
                  </View>
                  <View style={Styles.userView}>
                    <Text style={Styles.label}>Type</Text>
                    <Text style={Styles.label_data}>Bank</Text>
                  </View>
                  <View style={Styles.userView}>
                    <Text style={Styles.label}>
                      Account Number / Mobile Number
                    </Text>
                    <Text style={Styles.label_data}>
                      {item.bank_account_number}
                    </Text>
                  </View>
                  <View style={Styles.userView}>
                    <Text style={Styles.label}>Source Amount</Text>
                    <Text style={Styles.label_data}>{item.amount}</Text>
                  </View>
                  <View style={Styles.userView}>
                    <Text style={Styles.label}>Destination Amount</Text>
                    <Text style={Styles.label_data}>{item.calculated_amount}</Text>
                  </View>
                  <View style={Styles.userView}>
                    <Text style={Styles.label}>Status</Text>
                    <Text style={Styles.label_data}>
                      {item.gateway_message}
                    </Text>
                  </View>
                  {item.gateway_message != 'success' ? (
                    <CustomButton title="Make Payment" onPress={() => this.handleSendMoney(item.payment_url, item)}/>
                    // <TouchableOpacity
                    //   onPress={() => this.handleSendMoney(item.payment_url, item)}
                    //   style={Styles.paymentButtonStyle}
                    // >
                    //   <Text style={{ color: 'white' }}>Make Payment</Text>
                    // </TouchableOpacity>
                  ) : null}
                </View>
              )}
              keyExtractor={item => item.ref_no}
              // ItemSeparatorComponent={this.renderSeparator}
              // ListHeaderComponent={this.renderHeader}
              // ListFooterComponent={this.renderFooter}
              onRefresh={this.handleRefresh}
              refreshing={this.state.refreshing}
              // onEndReached={this.handleLoadMore}
              // onEndReachedThreshold={50}
            />
          </ScrollView>
        </View>
        {this.state.data.length < 1 ? (
          <View
            style={{ alignContent: 'center', flex: 1, alignSelf: 'center' }}
          >
            <Text style={{ fontSize: 18 }}>No Transactions Found</Text>
          </View>
        ) : null}
        {loading ?
          <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignContent: 'center', justifyContent: 'center' }}>
            <ActivityIndicator size="large" color="red" />
          </View>
          : 
          null
        }
      </SafeAreaView>
    );
  }
}

class RightNav extends React.Component {
  render() {
    return (
      <View style={{ width: '50%', alignSelf: 'center' }}>
        <TouchableOpacity
          style={Styles.navBtn}
          onPress={() => this.props.navigation.navigate('SendMoney')}
        >
          <Text style={{ color: 'white' }}>Send Money</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  navBtn: {
    padding: 10,
    backgroundColor: Constant.themeColor,
    borderRadius: 10,
    alignItems: 'center',
    marginBottom: 10
  },
  label: {
    flex: 0.5,
    fontWeight: 'bold'
  },
  label_data: {
    flex: 0.5,
    alignContent: 'flex-end'
  },
  iconStyle:{ 
		height: 30, 
		width: 30
	},
  item: {
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    margin: 10,
    backgroundColor: 'white',
    shadowOffset: { width: 5, height: 3 },
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOpacity: 1.0,
    elevation: 5,
    width: '90%',
    alignSelf: 'center',
    borderRadius: 10
  },
  userView: {
    flex: 1,
    margin: 5,
    flexDirection: 'row'
  },
  paymentButtonStyle: {
    width: 250,
    height: 40,
    backgroundColor: Constant.themeColor,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10
  },
  topImage: {
    paddingTop: 10,
    paddingBottom: 10
  },
  headerTitle: {
    padding: 5
  }
});

export default RecentTransactionsList;
