import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class OverlayView extends React.Component {
	render() {
		return(
			<View style={Styles.container}>
				<Text>Overlay</Text>
			</View>
		);
	}
}

const Styles = StyleSheet.create({
	container: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		elevation: 10,
		backgroundColor: 'black'
	}
});