import React from 'react';
import {View, Linking, Text, Button, Image, TouchableOpacity, StyleSheet, StatusBar, Alert, SafeAreaView} from 'react-native';
import CommonHeader from "../../components/CommonHeader";
import {CustomButton} from "../../components/StylesButton";
import Storage from "../../services/Storage";
import Constant from "../../services/Constants";
import Network from "../../services/NetworkRequest";

export default class DashboardScreen extends React.Component {
	state = {
		emailVerified: false,
		mobileVerified: false
	};

	componentDidMount() {
		Linking.getInitialURL().then((url) => {
			if(url) {
				console.log(url);
			}
		}, err => {
			console.log(err);
		}).catch(error => {
			console.log(error);
		})
		Storage.getData('userData').then(resp => {
			this.setState({
				user_id: resp.id,
				emailVerified: resp.email_verified,
				mobileVerified: resp.mobile_number_verified
			});
			if(resp.email_verified) {
				if(!resp.mobile_number_verified) {
					this.props.navigation.navigate('IdCheck');
					return;
				}
			}
		}, err => {
			console.log(err);
		});
	}
	handleSendMoney = () => {
		if(this.state.emailVerified) {
			this.props.navigation.navigate('SendMoney');
		} else {
			Alert.alert('Error', 'Please Verify Email');
		}
	}
	handleNewBeneficiary = () => {
		if(this.state.emailVerified) {
			this.props.navigation.navigate('Add');
		} else {
			Alert.alert('Error', 'Please Verify Email');
			return;
		}
	}
	handleViewTransactions = () => {
		if(this.state.emailVerified) {
			this.props.navigation.navigate('Transactions');
		} else {
			Alert.alert('Error', 'Please Verify Email');
		}
	}

	checkUserVerification = () => {
		let userId = this.state.user_id;
		Network.isEmailVerified(userId).then(resp => {
			console.log(resp);
			if(resp.success) {
				if(resp.data.user.email_verified) {
					Storage.setData('userData', resp.data.user).then(data => {
						console.log(data);
						this.setState({
							emailVerified: resp.data.user.email_verified
						}, () => {
							Alert.alert('Success', 'User has been verified', [{
								text: 'Verify Contact', onPress: () => this.props.navigation.navigate('IdCheck')
							}]);
						});
					}, err => {
						console.log(err);
					});
				} else {
					Alert.alert('Error', 'Email has not been verified');
					return;
				}
			} else {
				Alert.alert('Error', resp.errros.error);
			}
		}, err => {
			Alert.alert('Error', 'Server Error');
			console.log(err);
		});
	}

	render() {
		return(
			<SafeAreaView style={{flex: 1}}>
				<CommonHeader {...this.props}/>
				<View style={Styles.holder}>
					<Image
						style={{ height: 150, width: 150 }}
						source={require('../../assets/onboard.png')}
						/>
				</View>
				<View style={Styles.holder}>
					<View>
						{(this.state.emailVerified) ? 
						<View>
							<View>
								<CustomButton title="SEND MONEY" onPress={this.handleSendMoney}/>
							</View>
							<View>
								<CustomButton title="ADD BENEFICIARY" onPress={this.handleNewBeneficiary}/>
							</View>
							<View>
								<CustomButton title="VIEW TRANSACTIONS" onPress={this.handleViewTransactions}/>
							</View>
						</View>
						 : 
						 <View>
							<CustomButton title="Verify Email" onPress={this.checkUserVerification}/>
						</View>
						}
					</View>
				</View>
				{/* <OverlayView/> */}
			</SafeAreaView>
		);
	}
}

const Styles = StyleSheet.create({
  holder: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: 'white'
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 15,
    margin: 10,
    backgroundColor: Constant.themeColor,
    width: 200,
    borderRadius: 20,
    alignContent: 'center',
    alignItems: 'center'
  }
});
