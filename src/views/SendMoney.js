import React from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  TextInput,
  Alert,
  Linking,
  KeyboardAvoidingView,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Picker,
  Switch,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Network from '../services/NetworkRequest';
import Constant from '../services/Constants';
import CommonHeader from '../components/CommonHeader';
import Storage from '../services/Storage';
import PickerNew from "../components/Picker/picker.ios";

export default class SendMoney extends React.Component {
  state = {
    delivery_method: "CASH",
    selected_recipient: null,
    payoutCurrencies: null,
    sendingCurrencies: null,
    beneficiaryList: [],
    resetPicker: false,
    transaction_calculations: null,
    loading: false,
  };
  componentDidMount() {
    Storage.getData('userData').then(resp => {
      // console.log(resp);
			if(!resp.email_verified) {
				Alert.alert('Error', 'Please verify email.');
        this.props.navigation.navigate('Dashboard');
        return;
			} else if(!resp.mobile_number_verified) {
        this.props.navigation.navigate('IdCheck');
        return;
      } else if(resp.aasm_state != "accepted" && resp.aasm_state != "verified") {
        this.props.navigation.navigate('IdCheck');
        return;
      } else {
        this.getBeneficiaryList('CASH');
        this.getPayoutCurrencies();
      }
		}, err => {
			console.log(err);
    });
    // Linking.getInitialURL().then(url => {
    //   if (url) {
    //     console.log('Initial url is: ' + url);
    //   }
    // }).catch(err => console.error('An error occurred', err));
  }
  getBeneficiaryList = (item) => {
    if (item == 'BANK_DEPOSIT') {
      Network.beneficiaryListBank().then(resp => {
        // console.log('beneficiaryListBank------>', resp);
        if (resp.success && resp.data.beneficiary.length > 0) {
          this.setState({
            beneficiaryList: resp.data.beneficiary,
          });
        } else {
          Alert.alert('Error', resp.errors.error);
        }
      }, err => {
        Alert.alert('Error', 'Server Error');
        console.log(err);
      }).catch(error => {
        console.log(error);
      })
    }
    if (item == 'CASH') {
      Network.beneficiaryListMobile().then(resp => {
        console.log('beneficiaryListMobile------>', resp);
        if (resp.success && resp.data.beneficiary.length > 0) {
          this.setState({
            beneficiaryList: resp.data.beneficiary,
          });
        } else {
          Alert.alert('Error', resp.errors.error);
        }
      }, err => {
        Alert.alert('Error', 'Server Error');
        console.log(err);
      }).catch(error => {
        console.log(error);
      })
    }
  };
  getPayoutCurrencies = () => {
    this.setState({loading: true});
    Network.getAllCurrencies().then(resp => {
      console.log(resp);
      if ((resp.success, resp.data.payout_currencies.length > 0)) {
        this.setState({
          loading: false,
          payoutCurrencies: resp.data.payout_currencies,
        });
      } else {
        this.setState({
          loading: false
        })
        Alert.alert('Error', resp.message);
      }
    }, err => {
      this.setState({loading: false});
      Alert.alert('Error', 'Server Error.. Try Again Later');
      console.log(err);
    }).catch(err => {
      this.setState({loading: false});
      Alert.alert('Error', 'Server Error.. Try Again Later');
      console.log(err);
    });
  };
  // getSendingCurrencies = () => {
  //   this.setState({loading: true});
  //   Network.getSendingCurrencies().then(resp => {
  //     console.log(resp);
  //     if (resp.success && resp.data.sending_currencies.length > 0) {
  //       this.setState({
  //         loading: false,
  //         sendingCurrencies: resp.data.sending_currencies[0].name,
  //       });
  //     } else {
  //       Alert.alert('Client1', resp.message);
  //     }
  //   }, err => {
  //     this.setState({loading: false});
  //     console.log(err);
  //   }).catch(err => {
  //     this.setState({loading: false});
  //     console.log(err);
  //   });
  // };
  getCalculations = () => {
    let postDict = this.state;
    console.log(postDict);
    Network.getCurrencyCalculations(postDict).then(
      resp => {
        console.log(resp);
        if (resp.success) {
          this.setState({
            transaction_calculations: resp.data.transaction_calculations,
          });
        } else {
          Alert.alert('Error', resp.message);
        }
      },
      err => {
        //Alert.alert(),'Error: Serverside');
        console.log(err);
      },
    );
  };
  handleAmountInput(text) {
    let that = this;
    that.setState(
      {
        transfer_amount: text,
      },
      () => {
        if(that.state.transfer_amount > 0) {
          that.getCalculations();
        }
      },
    );
  }
  handleSendMoney = () => {
    let {transaction_calculations, selected_recipient, transfer_amount} = this.state;
    if(transfer_amount < 25 || transfer_amount > 4900 ) {
      Alert.alert('Error', 'Sorry. You are not authorized to make transaction of this amount');
      return;
    }
    if(transaction_calculations == null) {
      Alert.alert('Error', 'Please check details');
      return;
    }
    if (selected_recipient !== null) {
      this.setState({loading: true});
      let postDict = this.state;
      Network.sendMoney(postDict).then(resp => {
          console.log(resp);
          this.setState({loading: false});
          if (resp.success) {
            this.handleUrlCreate(resp.data);
          } else {
            this.setState({loading: false});
            Alert.alert('Error', resp.errors.error);
          }
        }, err => {
          this.setState({loading: false});
          Alert.alert('Error');
          console.log(err);
        }).catch(error => {
        console.log(error);
        Alert.alert('Error');
      });
    } else {
      return Alert.alert('Required Fields are missing');
    }
  };
  handleUrlCreate = (data) => {
    let orderData = data;
    Network.createLink(data).then(
      resp => {
        console.log(resp);
        if (resp.payment_url != null) {
          this.props.navigation.navigate('WebView', {url: resp.payment_url, orderDetails: orderData});
        } else {
          Alert.alert('Error', 'Unable to create url.');
        }
      },
      err => {
        console.log(err);
      },
    );
	};
	// handlePaymentMethod = (item) => {
	// 	this.setState({delivery_method: item});
	// 	this.getBeneficiaryList(item);
  // }
  handleDeliveryMethod = () => {
    if(this.state.delivery_method == "CASH") {
      this.setState({
        delivery_method: "BANK_DEPOSIT", 
        selected_recipient: "null",
        transaction_calculations: null,
        // beneficiaryList: [], 
        resetPicker: true
      }, () => {
        this.getBeneficiaryList("BANK_DEPOSIT");
      })
    } else {
      this.setState({
        delivery_method: "CASH", 
        selected_recipient: "null", 
        transaction_calculations: null,
        // beneficiaryList: [], 
        resetPicker: true
      }, () => {
        this.getBeneficiaryList("CASH");
      })
    }
  }
  handleSelectedBeneficiary = (item, state) => {
    let send_curr = this.state.payoutCurrencies[0]['sending_currency'];
    this.setState({
      resetPicker: state,
      sendingCurrencies: send_curr,
      selected_recipient: item
    });
  }
  render() {
    let {transaction_calculations, loading, beneficiaryList} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        <CommonHeader {...this.props} title="Send Money"/>
        <View style={{flex: 1}}>
          <View
            style={[Styles.topImage, {borderBottomWidth: 1, borderBottomColor: '#888881'}]}>
            <Image style={{height: 100, width: 100, alignSelf: 'center'}} source={require('../assets/beneficiary.png')}/>
            <View style={Styles.addSendMoneyViewStyle}>
              <Text style={Styles.addSendMoneyTextStyle}>Add Transaction</Text>
            </View>
          </View>
					<View style={{flex: 1}}>
						<KeyboardAvoidingView enabled behavior="height" keyboardVerticalOffset="200">
							<ScrollView>
								<View style={Styles.form}>
									<View style={Styles.formField}>
										<Text style={Styles.inputLabel}>Select Delivery Method</Text>
                    <View style={{flexDirection: 'row', marginVertical: 5}}>
                      <Switch 
                        value={(this.state.delivery_method == "BANK_DEPOSIT")} 
                        onChange={this.handleDeliveryMethod}>		
                      </Switch>
                      {(this.state.delivery_method == "BANK_DEPOSIT") 
                      ? <Text style={Styles.padding5}>Bank</Text> : <Text style={Styles.padding5}>Cash</Text>}
                    </View>
										{/* <Picker
											selectedValue={this.state.delivery_method}
											style={{height: 50, width: '100%'}}
											onValueChange={(item, itemIndex) => this.handlePaymentMethod(item)}>
											<Picker.Item label="Select" value="null" />
											<Picker.Item label="Bank Transfer" value="BANK_DEPOSIT" />
											<Picker.Item label="Cash Deposit" value="CASH" />
										</Picker> */}
									</View>
									<View style={Styles.formField}>
										<Text style={Styles.inputLabel}>Select Recipient</Text>
                    <PickerNew reset={this.state.resetPicker} selected={this.handleSelectedBeneficiary} list={beneficiaryList}/>
										{/* <Picker
											selectedValue={this.state.selected_recipient}
											style={{height: 50, width: '100%'}}
											onValueChange={(item, index) => {
                        let sendingCurrencies = this.state.payoutCurrencies[0]['sending_currency'];
                        this.setState({selected_recipient: item, sendingCurrencies: sendingCurrencies});
											}}>
											<Picker.Item label="SELECT BENEFICIARY" value="null" />
											{(this.state.beneficiaryList !== null && this.state.beneficiaryList.length > 0)
												? this.state.beneficiaryList.map((item, index) => (
														<Picker.Item
															key={index}
															label={item.first_name + ' ' + item.last_name}
															value={item}
														/>
													))
												: null}
										</Picker> */}
									</View>
									<View style={{flex: 1, flexDirection: 'row'}}>
										<View style={{flex: 0.5, padding: 10}}>
											<Text style={Styles.inputLabel}>Sending Currency</Text>
											<TextInput editable={false} value={this.state.payoutCurrencies !== null ? this.state.payoutCurrencies[0]['sending_currency'] : ''} style={Styles.formInput}/>
										</View>
										<View style={{flex: 0.5, padding: 10}}>
											<Text style={Styles.inputLabel}>Destination Currency</Text>
											<TextInput
												editable={false}
												value={this.state.selected_recipient !== null ? this.state.selected_recipient.currency : ''}
												style={Styles.formInput}>
											</TextInput>
										</View>
									</View>
                  {(this.state.delivery_method == "CASH") ? null : 
                    <View>
                      <View style={Styles.formField}>
                        <Text style={Styles.inputLabel}>Bank</Text>
                        <TextInput
                          editable={false}
                          value={this.state.selected_recipient !== null ? this.state.selected_recipient.bank_name : ''}
                          style={Styles.formInput}/>
                      </View>
                      <View style={Styles.formField}>
                        <Text style={Styles.inputLabel}>Account Number</Text>
                        <TextInput
                          editable={false}
                          value={this.state.selected_recipient !== null ? this.state.selected_recipient.bank_account_number : '' }
                          style={Styles.formInput}/>
                      </View>
                    </View>
                  }
									<View style={Styles.formField}>
										<Text style={Styles.inputLabel}>Transfer Amount</Text>
										<TextInput
											editable={this.state.loading == false}
											keyboardType="number-pad"
											onChangeText={text => this.handleAmountInput(text)}
											style={Styles.formInput}/>
									</View>
									<View style={Styles.formField}>
										<Text style={Styles.inputLabel}>Transfer Fee</Text>
										<TextInput
											editable={false}
											value={transaction_calculations !== null ? transaction_calculations.fixed_fee.toString() : '' }
											style={[Styles.formInput, {backgroundColor: '#7070700F'}]}
										/>
									</View>
									<View style={Styles.formField}>
										<Text style={Styles.inputLabel}>Total</Text>
                    <TextInput
											editable={false}
											value={transaction_calculations !== null ? transaction_calculations.total_amount.toString() : '' }
											style={[Styles.formInput, {backgroundColor: '#7070700F'}]}/>
									</View>
									<View style={Styles.formField}>
										<Text style={Styles.inputLabel}>Exchange Rate</Text>
										<TextInput
											editable={false}
											value={transaction_calculations !== null ? transaction_calculations.conversion_rate.toString() : '' }
											style={[Styles.formInput, {backgroundColor: '#7070700F'}]}/>
									</View>
									<View style={Styles.formField}>
										<Text style={Styles.inputLabel}>
											Your Beneficiary Will Receive..
										</Text>
                    <TextInput
											editable={false}
											value={transaction_calculations !== null ? transaction_calculations.calculated_amount.toString() : '' }
											style={[Styles.formInput, {backgroundColor: '#7070700F'}]}
										/>
									</View>
									<View style={{padding: 20}}>
										<TouchableOpacity
											onPress={this.handleSendMoney}
											style={Styles.sendMoney}>
											<Text style={{color: 'white'}}>SEND</Text>
										</TouchableOpacity>
									</View>
								</View>
							</ScrollView>
						</KeyboardAvoidingView>
					</View>
          {loading ? (
            <View
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                alignContent: 'center',
                justifyContent: 'center',
                backgroundColor: 'rgba(10,01,01,0.4)',
              }}>
              <ActivityIndicator size="large" color="red" />
            </View>
          ) : null}
        </View>
      </SafeAreaView>
    );
  }
}
const Styles = StyleSheet.create({
  addSendMoneyViewStyle: {
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  addSendMoneyTextStyle: {
    fontSize: 30,
    color: 'grey',
    fontWeight: 'bold',
  },

  formField: {
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 5,
  },
  inputLabel: {
    fontWeight: 'bold',
  },
  formInput: {
    borderWidth: 1,
    marginTop: 5,
    height: 40,
    borderRadius: 10,
    paddingLeft: 10,
    borderColor: Constant.borderGrey,
  },
  iconStyle: {
    height: 30,
    width: 30,
  },

  sendMoney: {
    width: 250,
    height: 40,
    backgroundColor: Constant.themeColor,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  padding5: {
    padding: 5,
    marginLeft: 10,
  },
  form: {
    width: '90%',
    alignSelf: 'center',
    borderWidth:1,
    borderColor: 'rgba(0,0,0,0.4)',
    backgroundColor: 'white',
    margin: 10,
    shadowOffset: {width: 5, height: 3},
    shadowColor: Constant.borderGrey,
    shadowOpacity: 1,
    elevation: 4,
    borderRadius: 10,
    paddingTop: 10,
	},
	topImage: {
		padding: 10
	}
});