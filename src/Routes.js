import React from 'react';
import { Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
// import DashboardScreen from "./views/Dashboard/Dashboard";

import HomeScreen from "./views/HomeRouter";

import IdRoutes from "./views/IDrouter";
// import IdCheckScreen from "./views/IdCheck";
import BeneficiaryStack from "./views/Brouter";
import SendMoneyScreen from './views/SendMoney';
import CustomDrawerContentComponent from './components/DrawerNavigation';
import WebViewScreen from "./views/WebViewScreen";
// import RecentTransactionsStack from "./views/Dashboard/RecentTransactionsList";

const DashboardNavigation = createDrawerNavigator({
  Dashboard: {screen: HomeScreen, navigationOptions: {
    drawerLabel: 'Home',
    drawerIcon: () => (<Image style={{height: 30, width: 30}} source={require('./assets/dashboard.png')}/>)
  }},
  IdCheck: {screen: IdRoutes, navigationOptions: {
    drawerLabel: 'ID Check',
    drawerIcon: () => (<Image style={{height: 30, width: 30}} source={require('./assets/kyc.png')}/>)
  }},
  Benificiary: {screen: BeneficiaryStack, navigationOptions: {
    drawerLabel: 'Beneficiary',
    drawerIcon: () => (<Image style={{height: 30, width: 30}} source={require('./assets/beneficiary.png')}/>)
  }},
  SendMoney: {screen: SendMoneyScreen, path: 'sendMoney/:status', navigationOptions: {
    drawerLabel: 'Send Money',
    drawerIcon: () => (<Image style={{height: 30, width: 30}} source={require('./assets/transactions.png')}/>)
  }},
  WebView: {screen: WebViewScreen, navigationOptions: {drawerLabel: () => null}}
}, 

{
  drawerWidth: 280,
  contentComponent: CustomDrawerContentComponent,
  unmountInactiveRoutes: true,
  contentOptions: {
    activeTintColor: '#469c17',
    itemsContainerStyle: {
      marginVertical: 0,
    },
    iconContainerStyle: {
      opacity: 1
    }
  }
});

export default createAppContainer(DashboardNavigation);