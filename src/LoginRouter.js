import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import LoginScreen from "./Login";
import SignUpScreen from "./SignUp";
import ForgetPasswordScreen from "./ForgetPassword";

const Navigation = createStackNavigator({
  Login: {screen: LoginScreen},
  Signup: {screen: SignUpScreen},
  ForgetPassword: {screen: ForgetPasswordScreen}
}, {
  headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }
});

export default createAppContainer(Navigation);