import React, { Component } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  View,
  Text,
  Modal,
  TextInput,
  Alert,
  Image,
  TouchableHighlight,
  TouchableOpacity
} from "react-native";
import Network from "../services/NetworkRequest";
import Storage from '../services/Storage';
import Constant from '../services/Constants';
import {CustomButton} from "../components/StylesButton";

export default class Form extends Component {
  state = {
    modalVisible: false,
    email: '',
    password: '',
    mobile: '',
    view: 'email',
    loading: false,
  };

  submitForm = () => {
    let {navigation} = this.props;
    let {email, password} = this.state;
    if(email == "" || password == "") {
      return Alert.alert('Error', 'Email and Password can not be empty');
    }
     else {
      this.setState({loading: true});
      this.props.result('true');
      Network.loginuser(this.state, 'email').then(resp => {
        console.log(resp);
        if(resp.success) {
          this.setState({loading: false});
          var response = Storage.setData('userData', resp.data.user);
          response.then(resp => {
            this.props.result('false');
            navigation.navigate('App');
          }, err => {
            this.setState({loading: false});
            this.props.result('false');
            console.log(err);
          });
        } else {
          this.setState({loading: false});
          this.props.result('false');
          Alert.alert('Error:', resp.errors.error);
        }
      }, err => {
        Constant.reportBug.notify(new Error("Login Error"));
        this.setState({loading: false});
          this.props.result('false');
          console.log(err);
          Alert.alert('Error', err);
      }).catch(err => {
        Constant.reportBug.notify(new Error("API Error"));
        this.setState({loading: false});
        console.log(err);
        Alert.alert('Error', err);
      });
    }
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }


  getOtp = () => {
    console.log("forms-----------> get otp service called")
    this.setState({loading: true});  
    var that = this;
    if(this.state.mobile == "") {
      this.setState({loading: false});
      Alert.alert('Error', 'Please fill mobile number');
      return;
    } 
    else {
      this.setState({loading: false});
      this.props.result('true');
      Network.getOtp(this.state.mobile, "login").then(resp => {
        if(resp.success) {
          console.log("get otp resp", resp);
          that.setModalVisible(true);
          that.setState({
            oading: false,
            optDetails: resp.data.verification_code
          });
        }
      }, 
      err => {
        this.setState({loading: false});
        console.log(" get otp  ", resp.errors.error)
        Alert.alert('Error', resp.errors.error);
        this.props.result('false');
      });
    }
  }


  submitOtp = () => {
    this.setState({loading: true});
    let that = this;
    let {navigation} = this.props;
    if(this.state.otp == "") {
      this.setState({loading: false});
      Alert.alert('Error', 'OTP is empty');
      return;
    } else {
      this.setState({loading: false});
      this.props.result('true');
      Network.loginuser(this.state, 'mobile').then(resp => {
        console.log(resp);
        if(resp.success) {
          this.setState({loading: false});
          that.setModalVisible(false);
          var response = Storage.setData('userData', resp.data.user);
          console.log(response);
          navigation.navigate('App');
        } else {
          that.setModalVisible(false);
          this.setState({loading: false});
          Alert.alert('Error', resp.errors.error);
        }
      }, err => {
        that.setModalVisible(false);
        this.setState({loading: false});
        console.log(err);
      });
       
      this.props.result('false');
    }
  }

  render() {
    let { loading } = this.state;
    const {navigation} = this.props;
    return (
      <View style={Styles.container}>
        {(this.state.view == 'mobile') ? 
          <View style={Styles.inputHolder}>
            <View style={{alignItems: 'center', justifyContent: 'center', alignSelf: 'center', padding: 20}}>
              <Image style={{width: 260, height: 50}} source={require('../assets/logo.png')}/>
            </View>

            
            
            <TextInput style={Styles.textInputStyle} placeholder="Mobile Number" keyboardType="number-pad" onChangeText={(mobile) => this.setState({mobile})}/>
            <TouchableOpacity style={Styles.SubmitButtonStyle} activeOpacity={0.5} onPress={this.getOtp}>
              <Text style={Styles.buttonTextStyle}>GET OTP</Text>
            </TouchableOpacity>
            <TouchableOpacity style={Styles.CancelStyleButton} activeOpacity={0.5} onPress={() => this.setState({view: 'email'})}>
              <Text style={Styles.cancelbuttonTextStyle}>
                SIGN IN WITH EMAIL
              </Text>
            </TouchableOpacity>
          </View>
          :
          <View style={Styles.inputHolder}>
            <View style={{alignItems: 'center', justifyContent: 'center', alignSelf: 'center', padding: 30}}>
              <Image style={{width: 260, height: 50}} source={require('../assets/logo.png')}/>
            </View>
            <TextInput editable={!loading} style={Styles.textInputStyle} placeholder="Email" autoCapitalize="none" keyboardType="email-address" onChangeText={(email) => this.setState({email})}/>
            <TextInput editable={!loading} style={Styles.textInputStyle} placeholder="Password" secureTextEntry={true} onChangeText={(password) => this.setState({password})}/>
            <View style={{display: 'flex', alignSelf: 'flex-end'}}>
              <TouchableOpacity style={{padding: 10}} onPress={() => navigation.navigate('ForgetPassword')}>
                <Text style={[Styles.textStyle, {textAlign: 'right', color: 'red'}]}>
                Forgotten your Password?
                </Text>
              </TouchableOpacity>
            </View>
            <CustomButton title="SIGN IN" onPress={this.submitForm.bind(this)}/>
            {/* <TouchableOpacity style={[Styles.SubmitButtonStyle, loading ? {backgroundColor: 'grey'}: {}]} activeOpacity={0.5} onPress={this.submitForm.bind(this)}>
              <Text style={Styles.buttonTextStyle}> SIGN IN </Text>
            </TouchableOpacity> */}
            {/* <TouchableOpacity style={Styles.CancelStyleButton} activeOpacity={0.5} onPress={() => this.setState({view: 'mobile'})}>
              <Text style={Styles.cancelbuttonTextStyle}>
                SIGN IN WITH MOBILE NUMBER
              </Text>
            </TouchableOpacity> */}
          </View>
        }
        <Modal animationType="slide" transparent={true} visible={this.state.modalVisible} onRequestClose={() => {Alert.alert('Modal has been closed.');}}>
          <View style={{flex: 1,flexDirection: 'column',justifyContent: 'center',alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.5)'}}>
            <View style={{height: 300, padding: 50, width: '80%', backgroundColor: 'white', alignSelf: 'center', justifyContent: 'center', alignItems: 'center'}}>
              <TextInput style={Styles.textInputStyle} placeholder="Enter OTP" keyboardType="number-pad"/>
              <TouchableOpacity style={Styles.SubmitButtonStyle} onPress={this.submitOtp}>
                <Text style={{color: 'white', textAlign: 'center'}}>SUBMIT OTP</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                <Text>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <View style={Styles.bottomHolder}>
          <Text>New User ?</Text>
          <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.navigate('Signup')}>
            <Text style={[Styles.textStyle, {color: 'red'}]}>
              Signup
            </Text>
          </TouchableOpacity>
        </View>
        {(loading) ? 
        <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignContent: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0)'}}>
          <ActivityIndicator size="large" color="red"/>
        </View>
        :
        null
        }
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center"
  },
  bottomHolder: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  inputHolder: {
    flex: 1,
    width: '100%',
    alignItems: "center",
    justifyContent: "center"
  },
  textInputStyle: {
    width: '100%',
    marginTop: 10,
    height: 40,
    padding: 5,
    paddingLeft: 15,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Constant.borderGrey,
  },
  CancelStyleButton: {
    width: '100%',
    marginTop: 10,
    paddingTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 10,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: Constant.themeColor
  },
  SubmitButtonStyle: {
    width: '100%',
    marginTop: 10,
    paddingTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 15,
    backgroundColor: Constant.themeColor,
    marginTop: 40,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#fff"
  },
  textButtonStyle: {
    width: 300,
    height: 40,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#75d472",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#fff"
  },
  cancelbuttonTextStyle: {
    color: Constant.themeColor,
    textAlign: "center",
    fontWeight: "800"
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "800"
  }
});
