import React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import Constant from "../services/Constants";

export const CustomButton = (props) => {
	const { title = 'Enter', style = {}, textStyle = {}, onPress } = props;

	return (
		<TouchableOpacity onPress={onPress} style={[styles.button, style]}>
			<Text style={[styles.text, textStyle]}>{props.title}</Text>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	button: {
    paddingHorizontal: 20,
    paddingVertical: 15,
	margin: 10,
	alignSelf: 'center',
    backgroundColor: Constant.themeColor,
    width: 200,
    borderRadius: 20,
    alignContent: 'center',
    alignItems: 'center',
		shadowColor: 'black',
		shadowOpacity: 0.6,
		shadowOffset: { height: 5, width: 0 },
		shadowRadius: 2,
	},
	text: {
		fontSize: 14,
		textTransform: 'uppercase',
		color: '#FFFFFF',
	},
});