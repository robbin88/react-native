const validation = {
	email: {
		presence: {
			message: '^Please enter an email address'
		},
		email: {
			message: '^Please enter a valid email address'
		}
	},
	password: {
		presence: {
			message: '^Please enter a password'
		},
		length: {
			minimum: 5,
			message: '^Your password must be at least 5 characters'
		}
	},
	matchpassword: {
		presence: {
			message: '^Please confirm a password'
		},
		match: {
			message: '^Password should match'
		}
	}
}

export default function validate(fieldName, value) {
	var formValues = {};
	formValues[fieldName] = value;
	var formFields = {};
	formFields[fieldName] = validation[field];
	const result = validate(formValues, formFields)
	if (result) {
    return result[field][0]
	}
	return null
}