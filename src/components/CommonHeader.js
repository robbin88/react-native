import React from "react";
import {Image, View, Text, TouchableOpacity, StyleSheet, Alert} from 'react-native';
import Storage from "../services/Storage";

export default class CommonHeader extends React.Component {
    state = {
        loadingValue: 0
};
    handleDrawerToggle = () => {
        this.props.navigation.toggleDrawer(); }
    
   render() {
        return(
            <View style={[Styles.header, this.props.style]}>
                <View style={Styles.leftNav}>
                    <TouchableOpacity onPress={this.handleDrawerToggle}>
                        <Image style={{height: 40, width: 40}} source={require('../assets/menu-icon.png')}/>
                    </TouchableOpacity>
                    <Text style={{padding: 10, marginLeft: 20, fontWeight: 'bold'}}>{(this.props.title) ? this.props.title : 'Home'}</Text>
                </View>
                <View style={Styles.rightNav}>
                    {this.props.rightOption}
                </View>
                <View style={[Styles.loadingScreen, {width: this.state.loadingValue+'%'}]}></View>
            </View>
        );
    }
}

const Styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        width: '100%',
        padding: 10,
        elevation: 5,
        backgroundColor: 'white',
    },
    loadingScreen: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 5,
        width: '120%',
        backgroundColor: 'orange'
    },
    leftNav: {
        flex: 0.5,
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    rightNav: {
        flex: 0.5,
        alignItems: 'flex-end'
    }
})