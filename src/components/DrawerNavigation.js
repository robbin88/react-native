import React from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  Button,
  ScrollView
} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import { DrawerNavigatorItems } from 'react-navigation-drawer';
import Storage from '../services/Storage';
import Constant from '../services/Constants';
 
export default class CustomDrawerContentComponent extends React.Component {
  state = {
    userData: null
  };
  handleLogout = () => {
    Storage.clearData().then(resp => {
      console.log(resp);
      if (resp == 'success') {
        this.props.navigation.navigate('Login');
      }
    }, err => {
      console.log(err);
    });
  };
  setUserData = () => {
    Storage.getData('userData').then(resp => {
      this.setState({
        userData: resp
      });
    }, err => {
      console.log(err);
    })
  }
  // static getDerivedStateFromProps(nextProps, prevState) {
  //   if (nextProps.total !== prevState.total) {
  //     return ({ total: nextProps.total }) // <- this is setState equivalent
  //   }
  // }
  componentWillReceiveProps() {
    this.setUserData();
  }
  render() {
    return (
      <ScrollView>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
          <View style={{ alignItems: 'center' }}>
            <View style={{ paddingVertical: 50 }}>
              <Image
                style={{ height: 40, width: 200 }}
                source={require('../assets/logo.png')}
              />
            </View>
            {(this.state.userData !== null) ? 
              <Text style={{fontWeight: 'bold', color: 'white', backgroundColor: Constant.themeColor, paddingHorizontal: 20, paddingVertical: 10, width: '100%', textAlign: 'center'}}>Hi {this.state.userData.first_name} {this.state.userData.last_name}</Text>
              :
              null
            }
          </View>
          <DrawerNavigatorItems style={{ backgroundColor: 'white' }} {...this.props}/>
          <TouchableOpacity style={{justifyContent: 'flex-end', padding: 15, flexDirection: 'row', alignSelf: 'flex-start' }} onPress={this.handleLogout}>
            <Image style={{ height: 30, width: 30 }} source={require('../assets/logout.png')}/>
            <Text style={{ fontWeight: 'bold', marginLeft: 25 }}>Logout</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
