import React from 'react';
import { View, Text } from "react-native";

export default class Picker extends React.Component {
	state = {
		list: [],
		selectedValue: null
	};
	componentWillMount() {
		let data = this.props.list;
		this.setState({
			list: data
		});
	}
	render() {
		return(
			<View>
				<Text>{this.props.selectorTitle}</Text>
				<Picker
					selectedValue={selectedValue}
					style={{ height: 50, width: '100%' }}
					onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue: itemValue, list: itemValue.states })}>
					<Picker.Item label={this.props.selectorTitle} value="null" />
					{(list != null) ? list.map((item, index) => (
							<Picker.Item key={index} label={item.name} value={item} />
					))
					:
					null}
				</Picker>
			</View>
		);
	}
}