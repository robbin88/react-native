import React from 'react';
import { ActionSheetIOS, ScrollView, TouchableOpacity, View, StyleSheet, Text, Modal } from "react-native";


export default class Picker extends React.Component {
	state = {
		list: [],
		selected: '',
		modalVisible: false,
	};
	setModalVisible = (status) => {
		this.setState({
			modalVisible: status
		});
	}
	static getDerivedStateFromProps(nextProps, prevState) {
		// console.log(nextProps);
		// console.log(prevState);
		if(nextProps.list) {
			if(nextProps.reset) {
				console.log('selected');
				return {selected: '', list: nextProps.list};
			}
			return {list: nextProps.list};
		} else {
			return null;
		}
 }
 handleBeneficiarySelection = (item) => {
	 let name = item.first_name+ ' '+item.last_name;
	 console.log(name);
	this.setState({
		selected: name,
		modalVisible: false
	}, () => {
		this.props.selected(item, false);
	});
 }
	render() {
		let {list} = this.state;
		return(
			<View style={{marginTop: 5, flex: 1, justifyContent: 'center'}}>
				<Modal
					animationType="slide"
					transparent={true}
					visible={this.state.modalVisible}
					onRequestClose={() => {
						Alert.alert('Modal has been closed.');
					}}>
					<View style={{flex: 1, marginTop: 22, height: '80%', width: '100%', alignSelf: 'center', justifyContent: 'center', backgroundColor: 'rgba(20,20,20,0.5)'}}>
						<View style={{padding: 20, backgroundColor: '#4a4a4a'}}>
							<Text style={{color: 'white', textAlign: 'center'}}>SELECT BENEFICIARY</Text>
						</View>
						<ScrollView showsVerticalScrollIndicator={false} style={[Styles.viewStyle]} contentContainerStyle={{backgroundColor: 'white'}}>
							{(list != undefined && list.length > 0) ? list.map((item, index) => {
								return(
									<TouchableOpacity style={Styles.itemBlock} key={index} onPress={() => this.handleBeneficiarySelection(item)}>
										<View>
											<Text>{item.first_name} {item.last_name}</Text>
										</View>
									</TouchableOpacity>
								);
							}) : null }
						</ScrollView>
						<TouchableOpacity
							onPress={() => {
								this.setModalVisible(!this.state.modalVisible);
							}} style={{padding: 20, backgroundColor: '#4a4a4a'}}>
							<Text style={{color: 'white', textAlign: 'center'}}>CANCEL</Text>
						</TouchableOpacity>
					</View>
				</Modal>
				<TouchableOpacity
					onPress={() => {
						this.props.selected(null, false);
						this.setModalVisible(true);
					}} style={{padding: 10, backgroundColor: '#ccc'}}>
					<Text>{(this.state.selected != '') ? this.state.selected : 'Select Beneficiary'}</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

const Styles = StyleSheet.create({
	viewStyle: {
		flex: 1,
		elevation: 10,
		backgroundColor: 'white'
	},
	itemBlock: {
		padding: 10,
		alignItems: 'center',
		borderBottomWidth: 1,
		borderBottomColor: '#e9e9e9'
	}
})