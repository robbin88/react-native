import React from "react";
import {
  StyleSheet,
  View,
  KeyboardAvoidingView
} from "react-native";
import Form from "./components/Form";
import Network from './services/NetworkRequest';
import Storage from "./services/Storage";

export default class LoginScreen extends React.Component {
  state = {
    view: 'mobile',
    loading: false
  };
  handleResult = (status) => {
    this.setState({
      loading: status
    });
  }
  componentWillUnmount() {
    let data = Network.getToken();
    console.log(data);
  }
  render() {
    let {loading} = this.state;
    return (
      <View style={Styles.container}>
        <KeyboardAvoidingView behavior="height" enabled>
          <Form {...this.props} type="Login" result={this.handleResult} />
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "flex-start"
  }
});
