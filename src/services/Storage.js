import AsyncStorage from '@react-native-community/async-storage';

const Storage = {
    setData: async(type, data) => {
        var response;
        try {
            await AsyncStorage.setItem(type, JSON.stringify(data));
            response = 'success';
        } catch(err) {
            response = err;
        }
        return response;
    },
    getData: async(type) => {
        try {
            let data = await AsyncStorage.getItem(type);
            return JSON.parse(data);
        } catch(err) {
            console.log(err.message);
        }
    },
    clearData: async() => {
        try {
            AsyncStorage.clear();
            return 'success';
        } catch(err) {
            return err;
        }
    },
    token: null
};


module.exports = Storage;