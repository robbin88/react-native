import { Client } from 'bugsnag-react-native';

const bugsnag = new Client("14cc772cd4f1dbc95df0e11857676565");

const Constants = {
    baseUrl: 'https://stage.roltex-pay.co.uk/',
    authToken: null,
    themeColor: '#4a4a4a',
    borderGrey: '#707070',
    AppName: 'Roltex',
    reportBug: bugsnag
};

module.exports = Constants;
