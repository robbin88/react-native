import Constants from './Constants';
import Storage from './Storage';
let BaseUrl = Constants.baseUrl;
let token = null;
let user_id = null;

function getUserData() {
  return new Promise((resolve, reject) => {
    Storage.getData('userData').then(
      resp => {
        resolve(resp);
      },
      err => {
        reject(err);
        console.log(err);
      },
    );
  });
}

const Network = {
  token: '',
  user_id: '',
  user_country: '',
  getToken: function() {
    return getUserData().then(
      resp => {
        console.log(resp);
        if (resp != null) {
          this.token = resp.auth_token;
          this.user_id = resp.id;
          this.user_country = resp.country;
        }
        return true;
      },
      err => {
        console.log(err);
        return false;
      },
    );
  },
  loginuser: function(data, option) {
    let user = convertToLogin(data, option);
    if (option == 'mobile') {
      var postDict = {
        user,
        verification_code_id: data.optDetails.id,
        otp: data.optDetails.code,
      };
    } else {
      var postDict = {user};
    }
    console.log(postDict);
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/api_login', {
        method: 'POST',
        body: JSON.stringify(postDict),
        headers: {'content-type': 'application/json'},
      })
        .then(resp => {
          console.log(resp);
          if(resp.status > 500) {
            throw new Error('Server Error');
          }
          return resp.json();
        })
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  forgetPassword: function(data) {
    let postData = new FormData();
    postData.append('user[email]', data);
    console.log(postData);
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/forgot_password', {
        method: 'POST',
        body: postData,
        headers: {'content-type': 'multipart/form-data'},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },

  getOtp: function(data, purpose) {
    console.log('get otp reque---> ', data, purpose);
    let postData = {
      mobile_number: data,
      registered_via: 'Mobile',
      purpose: purpose,
    };
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/verification_codes', {
        method: 'POST',
        body: JSON.stringify({verification_code: postData}),
        headers: {'content-type': 'application/json'},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  verfiyOtp: function(data) {
    var postDict = {
      verification_code: {code: data.otp},
      id: data.otpID,
    };
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/verification_codes/' + data.otpID, {
        method: 'PUT',
        body: JSON.stringify(postDict),
        headers: {
          'content-type': 'application/json',
          Authorization: this.token,
        },
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  isEmailVerified: function(userId) {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/users/' + userId, {
        headers: {
          'content-type': 'application/json',
          Authorization: this.token,
        },
      }).then(resp => resp.json()).then(respJson => {
          resolve(respJson);
        })
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  signupUser: function(data, option) {
    console.log(data);
    let postData = convertToSignup(data, option);

    console.log('mobile number----> ', postData);

    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/registrations', {
        method: 'POST',
        body: JSON.stringify({user: postData}),
        headers: {'content-type': 'application/json'},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },

  recentTransactions: function(data) {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/transactions/index_by_user', {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },

  recentBenificiary: function(data) {},

  getOccupations: function(data) {
    this.getToken();
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/occupations', {
        headers: {Authorization: this.token},
      })
        .then(resp =>  {
          if(resp.status > 400 ){
            throw new Error('Something Went Wrong!')
          }
          return resp.json()
        })
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
getAllCountries: function() {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/countries', {headers: {Authorization: this.token}})
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  getAllBanks: function(data) {
    this.getToken();
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/banks', {headers: {Authorization: this.token}})
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  getAllCurrencies: function() {
    this.getToken();
    console.log(this.token);
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/users/payout_currencies', {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  getCurrencyId: function(id) {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/conversion_rates/get_currency?name=' + id, {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  getBankForSelectedCurrency: function(id) {
    this.getToken();
    console.log(id);
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/banks/index_by_currency?currency_id=' + id, {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  getAllProviders: function(id, country_id) {
    this.getToken();
    var getParams = 'currency_id='+id+'&sending_country='+this.user_country;
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/providers/index_by_currency?'+getParams, {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  getLocation: function(data) {},
  getStates: function() {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/states', {headers: {Authorization: this.token}})
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  getSendingCurrencies: function() {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/users/sending_currencies', {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  getCurrencyCalculations: function(data) {
    let paramstring =
      '?sending_currency=' +
      data.sendingCurrencies +
      '&destination_currency=' +
      data.selected_recipient.currency +
      '&amount=' +
      data.transfer_amount +
      '';
    console.log(paramstring);
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/calculations/show' + paramstring, {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  updateUserDetails: function(data) {
    let user_id = this.user_id;
    let postDict = createUserUpdate(data);
    console.log(postDict);
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/users/' + user_id, {
        method: 'PUT',
        body: JSON.stringify(postDict),
        headers: {
          Authorization: this.token,
          'content-type': 'application/json',
        },
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
        .catch(err => reject(err));
    });
  },
  getDocumentType: function(type) {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/document_types/index_by_name?name=' + type, {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  uploadDocument: function(data) {
    let postData = createFileUpload(data);
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/user_documents/upload_file', {
        method: 'POST',
        body: postData,
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: this.token,
        },
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
        .catch(err => reject(err));
    });
  },
  submitDocuments: function(data) {
    this.getToken();
    let postDict = {user_documents: formatSubmitDocuments(data, this.user_id)};
    console.log(JSON.stringify(postDict));
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/user_documents/bulk_create', {
        method: 'POST',
        body: JSON.stringify(postDict),
        headers: {
          Authorization: this.token,
          'content-type': 'application/json',
        },
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  beneficiaryListBank: function() {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/beneficiaries/filtered_index_by_bank', {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  beneficiaryListMobile: function() {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/beneficiaries/filtered_index_by_cash', {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  beneficiaryList: function() {
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/beneficiaries/index_by_user', {
        headers: {Authorization: this.token},
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  addBenificiary: function(data) {
    this.getToken();
    console.log('parameter for beneficiary---->', data);
    let returnData = formatAddBeneficiaryData(data);
    let postDict = {beneficiary: returnData};

    console.log('parameter for beneficiary---->', returnData);
    console.log('parameter for postDict---->', postDict);
    console.log('parameter for JSON', JSON.stringify(postDict));

    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/beneficiaries', {
        method: 'POST',
        body: JSON.stringify(postDict),
        headers: {
          Authorization: this.token,
          'content-type': 'application/json',
        },
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
				// .catch(err => err.text().then(errorMessage => reject(errorMessage)));
				.catch(err => new Error(err)).then(errorMessage => reject(errorMessage))
    });
  },
  sendMoney: function(data) {
    let user_id = this.user_id;
    var postDict = formatSendMoney(data);
    postDict.transaction.user_id = user_id;
    console.log(postDict);
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/transactions/custom_create', {
        method: 'POST',
        body: JSON.stringify(postDict),
        headers: {
          Authorization: this.token,
          'content-type': 'application/json',
        },
      })
        .then(resp => {
          if(resp.status > 400 ){
            throw new Error('Something Went Wrong!')
          }
          return resp.json()
        })
        .then(respJson => resolve(respJson))
        .catch(err => reject(err));
    });
  },
  createLink: function(postDict) {
    console.log(postDict);
    return new Promise((resolve, reject) => {
      fetch(BaseUrl + 'mobile/v1/transactions/create_payment_url', {
        method: 'POST',
        body: JSON.stringify({id: postDict.transaction.id}),
        headers: {
          Authorization: this.token,
          'content-type': 'application/json',
        },
      })
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
        .catch(err => reject(err));
    });
  },
  getUserDocuments: function() {
    let user_id = this.user_id;
    return new Promise((resolve, reject) => {
      fetch(
        BaseUrl + 'mobile/v1/user_documents/index_by_user?user_id=' + user_id,
        {headers: {Authorization: this.token}},
      )
        .then(resp => resp.json())
        .then(respJson => resolve(respJson))
        .catch(err => reject(err));
    });
  },
};

Network.getToken();

function formatSendMoney(data) {
  console.log(data);
  var postData = {
    transaction: {
      amount: data.transfer_amount,
      sending_currency: data.sendingCurrencies,
      delivery_method: data.delivery_method,
      fixed_fee: data.transaction_calculations.fixed_fee,
      beneficiary_id: data.selected_recipient.id,
      conversion_rate: data.transaction_calculations.conversion_rate,
      calculated_amount: data.transaction_calculations.calculated_amount,
      paid_amount: data.transaction_calculations.total_amount,
      beneficiary_relation: data.selected_recipient.relation,
    },
  };
  return postData;
}

function createUserUpdate(data) {
  let postDict;
  var t = dataSetCheck(data);
  console.log(t);
  if(t) {
    if(data.aasm_state) {
      postDict = {
        aasm_state: data.aasm_state
      };
    } else {
      postDict = {
        email: data.email || '',
        first_name: data.first_name || '',
        last_name: data.last_name || '',
        address: data.address || '',
        city: data.city || '',
        zipcode: data.zipcode || '',
        country: data.selectedCountry ? data.selectedCountry.name : '',
        state: data.selectedState ? data.selectedState.name : '',
        mobile_number: data.mobile_number || '',
        occupation_id: data.selectedOccupation ? data.selectedOccupation : '',
        state_id: data.selectedState ? data.selectedState.id : '',
      };
      if(data.date_of_birth != null || data.date_of_birth != undefined) {
        var d = data.date_of_birth.toLocaleDateString();
        var ot = d.split('/');
        postDict.birth_date = ot[2] + '-' + ot[1] + '-' + ot[0] || '';
      }
      for (let v in postDict) {
        if (postDict[v] == '' || postDict[v] == undefined) {
          delete postDict[v];
        }
      }
    }
  } else {
    postDict = {
      email: data.email || '',
      first_name: data.first_name || '',
      last_name: data.last_name || '',
      address: data.address || '',
      city: data.city || '',
      zipcode: data.zipcode || '',
      country: data.selectedCountry ? data.selectedCountry.name : '',
      state: data.selectedState ? data.selectedState.name : '',
      mobile_number: data.mobile_number || '',
      occupation_id: data.selectedOccupation ? data.selectedOccupation : '',
      state_id: data.selectedState ? data.selectedState.id : '',
    };
    if(data.date_of_birth != null || data.date_of_birth != undefined) {
      var d = data.date_of_birth.toLocaleDateString();
      var ot = d.split('/');
      postDict.birth_date = ot[2] + '-' + ot[1] + '-' + ot[0] || '';
    }
    for (let v in postDict) {
      if (postDict[v] == '' || postDict[v] == undefined) {
        delete postDict[v];
      }
    }
  }
  console.log('createUserUpdate---->', {user: postDict});
  return {user: postDict};
}

function dataSetCheck(data) {
  var userObj = data;
  console.log(userObj);
	delete userObj.id, 
	delete userObj.elemental_docket_id, 
	delete userObj.elemental_docket_upload_status, 
	delete userObj.emailage_score, 
	delete userObj.confirmation_token, 
	delete userObj.response_message, 
	delete userObj.gateway_message, 
	delete userObj.auth_token, 
	delete userObj.client;
	var response = true;
	for(var t in userObj) {
		if(userObj[t] == '' || userObj[t] == null) {
			response = false;
		}
	}
	return response;
}

function createFileUpload(data) {
  console.log('createFileUpload--->', data);
  var postDict = new FormData();
  postDict.append('files', {
    uri: data.uri,
    name: 'photo.png',
    filename: 'imageName.png',
    type: 'image/png',
  });
  postDict.append('Content-Type', 'image/png');
  console.log(postDict);
  return postDict;
}

function formatSubmitDocuments(data, user_id) {
  console.log('formatSubmitDocuments---->', data);

  let user_documents_meta = [],
    identity_doc = {},
    identity_doc2 = {};
  for (var r = 0; r < data.metaData.length; r++) {
    if (data.metaData[r]['content_key'] == 'Expiration Date') {
      var date = data.expiryDate.toLocaleDateString();
      var new_d = date.split('/');
      user_documents_meta.push({
        metacontent_id: data.metaData[r]['id'],
        content_value: new_d[2] + '-' + new_d[1] + '-' + new_d[0],
      });
    }
    if (data.metaData[r]['content_key'] == 'Issuer Country') {
      user_documents_meta.push({
        metacontent_id: data.metaData[r]['id'],
        content_value: data.issuer_country,
      });
    }
    if (data.metaData[r]['content_key'] == 'Id Number') {
      user_documents_meta.push({
        metacontent_id: data.metaData[r]['id'],
        content_value: data.id_number,
      });
    }
    if (data.metaData[r]['content_key'] == 'Date of Issue') {
      var date = data.issueDate.toLocaleDateString();
      var new_d = date.split('/');
      user_documents_meta.push({
        metacontent_id: data.metaData[r]['id'],
        content_value: new_d[2] + '-' + new_d[1] + '-' + new_d[0],
      });
    }
    if (data.metaData[r]['content_key'] == 'Issued By') {
      user_documents_meta.push({
        metacontent_id: data.metaData[r]['id'],
        content_value: data.issuedby,
      });
    }
  }
  let fullData = [];
  if (data.docType) {
    for (var e = 0; e < data.identityDocList.length; e++) {
      if (data.identityDocList[e]['name'] === 'Driving License') {
        identity_doc.user_id = user_id;
        identity_doc.document_id = data.identityDocList[e]['id'];
        identity_doc.public_url = data.public_url_identity_2;
        identity_doc.face_type = 'FRONT';
        identity_doc.user_document_metacontents_attributes = user_documents_meta;

        identity_doc2.user_id = user_id;
        identity_doc2.document_id = data.identityDocList[e]['id'];
        identity_doc2.public_url = data.public_url_identity_1;
        identity_doc2.face_type = 'BACK';
        identity_doc2.user_document_metacontents_attributes = user_documents_meta;
        fullData.push(identity_doc, identity_doc2);
      }
    }
  } else {
    for (var e = 0; e < data.identityDocList.length; e++) {
      if (data.identityDocList[e]['name'] === 'Passport') {
        identity_doc.document_id = data.identityDocList[e]['id'];
        identity_doc.public_url = data.public_url_identity_2;
        identity_doc.face_type = 'FRONT';
        identity_doc.user_id = user_id;
        identity_doc.user_document_metacontents_attributes = user_documents_meta;
        fullData.push(identity_doc);
      }
    }
  }
  address_proof = {
    user_id: user_id,
    document_id: data.selectedAddressProof,
    public_url: data.public_url_kyc,
    face_type: 'FRONT',
    user_document_metacontents_attributes: [],
  };
  fullData.push(address_proof);
  console.log('fullData---->', fullData);
  var returnData = fullData;
  return returnData;
}

function formatAddBeneficiaryData(data) {
  var postDict = {
    first_name: data.first_name,
    last_name: data.last_name,
    currency: data.currency,
    city: data.city,
    user_id: data.user_id,
    email: data.email,
    mobile_number: data.mobile_number,
    zipcode: '10000',
    other_bank_name: data.beneficiary_other_bank_name,
    relation: data.relation
  };
  if (data.formType) {
    postDict.bank_name = data.selectedBank.name;
    postDict.bank_id = data.selectedBank.id;
    postDict.bank_account_number = data.bank_account_number;
    postDict.bank_account_type = 'SAVINGS';
    postDict.transaction_type = 'BANK';
  } else {
    postDict.bank_name = data.selectedProvider.name;
    postDict.bank_id = data.selectedProvider.id;
    postDict.bank_account_number = data.mobile_number;
    // postDict.bank_account_type = "CASH";
    postDict.transaction_type = 'CASH';
  }
  return postDict;
}

function convertToLogin(d, option) {
  console.log('convertToLogin---->', option);
  if (option == 'email') {
    var user = {
      email: d.email,
      registered_via: 'EMAIL',
      password: d.password,
    };
  } else {
    var user = {
      mobile_number: d.mobile,
      registered_via: 'MOBILE',
      purpose: 'login',
    };
  }
  console.log('convertToLogin---->', user);
  return user;
}

function convertToSignup(data, option) {
  if (option == 'mobile') {
    var user = {
      mobile_number: data.mobile,
      registered_via: 'MOBILE',
      verification_code_id: data.otpData.id,
      otp: data.otp,
    };
  } else {
    var user = {
      email: data.email,
      password: data.password,
      password_confirmation: data.confirmPassword,
      registered_via: 'EMAIL',
    };
  }
  return user;
}

module.exports = Network;
