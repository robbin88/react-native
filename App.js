/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import SplashRouter from "./src/SplashRouter";

const prefix = 'roltexapp://';

const App = () => {
  return (
    <SplashRouter uriPrefix={prefix}/>
  );
};

export default App;
